package info.owczarek.jps.database.ast;

public abstract class UnaryExpression extends Expression {
	protected Expression innerExpression;

	public UnaryExpression(Expression innerExpression) {
		this.innerExpression = innerExpression;
	}

	public Expression getInnerExpression() {
		return innerExpression;
	}

	protected String toString(String operatorSign) {
		return operatorSign + "(" + getInnerExpression() + ")";
	}
}
