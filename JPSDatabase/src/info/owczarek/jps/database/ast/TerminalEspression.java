package info.owczarek.jps.database.ast;

public abstract class TerminalEspression<T> extends Expression {
	protected T value;
	
	public T getValue() {
		return value;
	}
	
	public String toString() {
		return value.toString();
	}
}
