package info.owczarek.jps.database.ast.auxiliary;

import info.owczarek.jps.database.ast.*;

public class GroupAsExpression extends AuxiliaryNameExpression {
	public GroupAsExpression(String auxiliaryName, Expression innerExpression) {
		super(auxiliaryName, innerExpression);
	}

	@Override
	public String toString() {
		return "(" + getInnerExpression() + " group as " + getAuxiliaryName();
	}
}
