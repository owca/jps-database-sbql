package info.owczarek.jps.database.ast.auxiliary;

import info.owczarek.jps.database.ast.AuxiliaryNameExpression;
import info.owczarek.jps.database.ast.Expression;

public class AsExpression extends AuxiliaryNameExpression {

	public AsExpression(String auxiliaryName, Expression innerExpression) {
		super(auxiliaryName, innerExpression);
	}

	@Override
	public String toString() {
		return "(" + getInnerExpression() + " as " + getAuxiliaryName() + ")";
	}
}
