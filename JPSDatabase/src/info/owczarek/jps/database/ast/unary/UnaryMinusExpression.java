package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class UnaryMinusExpression extends UnaryExpression {
	public UnaryMinusExpression(Expression innerExpression) {
		super(innerExpression);
	}
	
	@Override
	public String toString() {
		return toString("-");
	}
}
