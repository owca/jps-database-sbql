package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class NotExpression extends UnaryExpression {
	public NotExpression(Expression innerExpression) {
		super(innerExpression);
	}

	@Override
	public String toString() {
		return toString("not");
	}
}
