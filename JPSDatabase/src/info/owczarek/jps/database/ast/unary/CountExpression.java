package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class CountExpression extends UnaryExpression {
	public CountExpression(Expression innerExpression) {
		super(innerExpression);
	}

	@Override
	public String toString() {
		return toString("count");
	}
}
