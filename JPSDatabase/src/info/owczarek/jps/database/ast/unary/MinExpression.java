package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class MinExpression extends UnaryExpression {
	public MinExpression(Expression innerExpression) {
		super(innerExpression);
	}
	
	@Override
	public String toString() {
		return toString("min");
	}
}
