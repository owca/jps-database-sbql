package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class UnaryPlusExpression extends UnaryExpression {
	public UnaryPlusExpression(Expression innerExpression) {
		super(innerExpression);
	}
	
	@Override
	public String toString() {
		return toString("+");
	}
}
