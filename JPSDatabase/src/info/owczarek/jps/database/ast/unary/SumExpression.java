package info.owczarek.jps.database.ast.unary;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.UnaryExpression;

public class SumExpression extends UnaryExpression {
	public SumExpression(Expression innerExpression) {
		super(innerExpression);
	}
	
	@Override
	public String toString() {
		return toString("sum");
	}
}
