package info.owczarek.jps.database.ast;

public abstract class BinaryExpression extends Expression {
	protected Expression leftExpression;
	protected Expression rightExpression;

	public BinaryExpression(Expression leftExpression, Expression rightExpression) {
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	public Expression getLeftExpression() {
		return leftExpression;
	}

	public Expression getRightExpression() {
		return rightExpression;
	}

	public String toString(String operatorSign) {
		return "(" + getLeftExpression().toString() + operatorSign + getRightExpression().toString() + ")";
	}
}
