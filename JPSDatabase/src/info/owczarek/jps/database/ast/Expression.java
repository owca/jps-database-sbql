package info.owczarek.jps.database.ast;

import info.owczarek.jps.database.ast.visitor.ASTVisitor;

public class Expression {
	public void accept(ASTVisitor visitor) {
		visitor.visit(this);
	}
}
