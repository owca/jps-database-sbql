package info.owczarek.jps.database.ast.terminal;

import info.owczarek.jps.database.ast.TerminalEspression;

public class IntegerTerminal extends TerminalEspression<Integer> {
	public IntegerTerminal(Integer value) {
		this.value = value;
	}
}
