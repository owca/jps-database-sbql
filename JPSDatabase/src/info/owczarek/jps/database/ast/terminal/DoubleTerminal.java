package info.owczarek.jps.database.ast.terminal;

import info.owczarek.jps.database.ast.TerminalEspression;

public class DoubleTerminal extends TerminalEspression<Double> {
	public DoubleTerminal(Double value) {
		this.value = value;
	}
}
