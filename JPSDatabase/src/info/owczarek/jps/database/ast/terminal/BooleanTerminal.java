package info.owczarek.jps.database.ast.terminal;

import info.owczarek.jps.database.ast.TerminalEspression;

public class BooleanTerminal extends TerminalEspression<Boolean> {
	public BooleanTerminal(Boolean value) {
		this.value = value;
	}
}
