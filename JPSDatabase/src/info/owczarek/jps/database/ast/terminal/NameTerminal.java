package info.owczarek.jps.database.ast.terminal;

import info.owczarek.jps.database.ast.TerminalEspression;

public class NameTerminal extends TerminalEspression<String> {
	public NameTerminal(String name) {
		this.value = name;
	}

	public String getName() {
		return value;
	}
}
