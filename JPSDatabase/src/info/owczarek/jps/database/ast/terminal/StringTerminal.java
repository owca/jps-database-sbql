package info.owczarek.jps.database.ast.terminal;

import info.owczarek.jps.database.ast.TerminalEspression;

public class StringTerminal extends TerminalEspression<String> {
	public StringTerminal(String value) {
		this.value = value;
	}
	
	public String toString() {
		return "\"" + value + "\"";
	}
}
