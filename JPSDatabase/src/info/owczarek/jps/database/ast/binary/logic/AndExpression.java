package info.owczarek.jps.database.ast.binary.logic;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class AndExpression extends BinaryExpression {
	public AndExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	@Override
	public String toString() {
		return toString(" and ");
	}
}
