package info.owczarek.jps.database.ast.binary.comparison;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class EqualExpression extends BinaryExpression {
	public EqualExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" == ");
	}
}
