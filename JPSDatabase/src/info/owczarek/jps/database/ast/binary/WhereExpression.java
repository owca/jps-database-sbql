package info.owczarek.jps.database.ast.binary;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class WhereExpression extends BinaryExpression {
	public WhereExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" where ");
	}
}
