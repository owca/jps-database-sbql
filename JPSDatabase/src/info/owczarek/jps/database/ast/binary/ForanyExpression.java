package info.owczarek.jps.database.ast.binary;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class ForanyExpression extends BinaryExpression {
	public ForanyExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	@Override
	public String toString() {
		return "(forany " + getLeftExpression().toString() + " " + getRightExpression().toString() + ")";
	}
}
