package info.owczarek.jps.database.ast.binary;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class MinusExpression extends BinaryExpression {
	public MinusExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	@Override
	public String toString() {
		return toString(" minus ");
	}
}
