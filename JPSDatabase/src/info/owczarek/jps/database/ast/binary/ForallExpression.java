package info.owczarek.jps.database.ast.binary;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class ForallExpression extends BinaryExpression {
	public ForallExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}

	@Override
	public String toString() {
		return "(forall " + getLeftExpression().toString() + " " + getRightExpression().toString() + ")";
	}
}
