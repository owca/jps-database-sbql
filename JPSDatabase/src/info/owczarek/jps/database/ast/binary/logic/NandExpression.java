package info.owczarek.jps.database.ast.binary.logic;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class NandExpression extends BinaryExpression {
	public NandExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" nand ");
	}
}
