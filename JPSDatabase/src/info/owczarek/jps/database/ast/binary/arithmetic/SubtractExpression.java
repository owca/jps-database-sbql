package info.owczarek.jps.database.ast.binary.arithmetic;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class SubtractExpression extends BinaryExpression {
	public SubtractExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" - ");
	}
}
