package info.owczarek.jps.database.ast.binary.logic;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class OrExpression extends BinaryExpression {
	public OrExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" or ");
	}
}
