package info.owczarek.jps.database.ast.binary;

import java.util.List;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.qres.AbstractQueryResult;
import info.owczarek.jps.database.qres.BagResult;
import info.owczarek.jps.database.qres.SimpleResult;
import info.owczarek.jps.database.qres.SingleResult;
import info.owczarek.jps.database.qres.StructResult;

public class ComaExpression extends BinaryExpression {
	public ComaExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(", ");
	}

	// 1, 2 -> struct(1, 2)
	public static StructResult cartesianProduct(SingleResult resLeft, SingleResult resRight) {
		StructResult result = new StructResult(resLeft, resRight);
		return result;
	}

	// struct(1, 2), 3 -> struct(1, 2, 3)
	public static StructResult cartesianProduct(StructResult resLeft, SimpleResult resRight) {
		List<AbstractQueryResult> elements = resLeft.getElements();
		SingleResult[] elementsArray = (SingleResult[]) elements.toArray();
		SingleResult[] resultArray = new SingleResult[elements.size() + 1];
		System.arraycopy(elementsArray, 0, resultArray, 0, elementsArray.length);

		resultArray[resultArray.length - 1] = resRight;
		StructResult result = new StructResult(resultArray);

		return result;
	}

	// struct(1, 2), 3 -> struct(1, 2, 3)
	public static StructResult cartesianProduct(SimpleResult resLeft, StructResult resRight) {
		List<AbstractQueryResult> elements = resRight.getElements();
		SingleResult[] elementsArray = (SingleResult[]) elements.toArray();
		SingleResult[] resultArray = new SingleResult[elements.size() + 1];

		resultArray[0] = resLeft;
		System.arraycopy(elementsArray, 0, resultArray, 1, elementsArray.length);

		StructResult result = new StructResult(resultArray);
		return result;
	}

	public static BagResult cartesianProduct(BagResult resLeft, BagResult resRight) {
		BagResult result = new BagResult();

		for (AbstractQueryResult bagL : resLeft.getElements()) {
			for (AbstractQueryResult bagR : resRight.getElements()) {
				result.addElement(new StructResult(bagL, bagR));
			}
		}

		return result;
	}

	public static BagResult cartesianProduct(SingleResult resLeft, BagResult resRight) {
		BagResult result = new BagResult();

		for (AbstractQueryResult bagR : resRight.getElements()) {
			result.addElement(new StructResult(resLeft, bagR));
		}

		return result;
	}

	public static BagResult cartesianProduct(BagResult resLeft, SingleResult resRight) {
		BagResult result = new BagResult();

		for (AbstractQueryResult bagL : resLeft.getElements()) {
			result.addElement(new StructResult(bagL, resRight));
		}

		return result;
	}
}
