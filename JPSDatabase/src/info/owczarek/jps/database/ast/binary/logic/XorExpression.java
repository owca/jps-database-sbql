package info.owczarek.jps.database.ast.binary.logic;

import info.owczarek.jps.database.ast.BinaryExpression;
import info.owczarek.jps.database.ast.Expression;

public class XorExpression extends BinaryExpression {
	public XorExpression(Expression leftExpression, Expression rightExpression) {
		super(leftExpression, rightExpression);
	}
	
	@Override
	public String toString() {
		return toString(" xor ");
	}
}
