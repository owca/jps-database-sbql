package info.owczarek.jps.database.ast;

public abstract class AuxiliaryNameExpression extends Expression {
	protected String auxiliaryName;
	protected Expression innerExpression;

	public AuxiliaryNameExpression(String auxiliaryName, Expression innerExpression) {
		this.auxiliaryName = auxiliaryName;
		this.innerExpression = innerExpression;
	}

	public Expression getInnerExpression() {
		return innerExpression;
	}

	public String getAuxiliaryName() {
		return auxiliaryName;
	}
}
