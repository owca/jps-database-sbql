package info.owczarek.jps.database.ast.visitor.operationExecutors;

import info.owczarek.jps.database.qres.*;

/**
 * Klasa odpowiedzialna za wykonanie operacji agregacji
 */
public abstract class AggregationOperatorPerformer {
	public void addElement(Object elementResultObject) {
		throw new RuntimeException("addElement has not been implemented for type Object");
	}

	public void addElement(Integer elementResultObject) {
		throw new RuntimeException("addElement has not been implemented for type Integer");
	}

	public void addElement(Double elementResultObject) {
		throw new RuntimeException("addElement has not been implemented for type Double");
	}

	public abstract AbstractQueryResult getResult();
}
