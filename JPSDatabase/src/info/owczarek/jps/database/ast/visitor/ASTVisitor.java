package info.owczarek.jps.database.ast.visitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.commons.lang3.StringUtils;

import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.auxiliary.*;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.arithmetic.*;
import info.owczarek.jps.database.ast.binary.comparison.*;
import info.owczarek.jps.database.ast.binary.logic.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.ast.visitor.operationExecutors.*;
import info.owczarek.jps.database.envs.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;

public class ASTVisitor {
	private SBAStore sbaStore;
	private QResStack qres;
	private ENVS envs;

	public ASTVisitor(SBAStore sbaStore, QResStack qres, ENVS envs) {
		this.sbaStore = sbaStore;
		this.qres = qres;
		this.envs = envs;
	}

	public void visit(Expression expression) {
		if (expression instanceof TerminalEspression<?>) {
			visit((TerminalEspression<?>) expression);
		} else if (expression instanceof BinaryExpression) {
			visit((BinaryExpression) expression);
		} else if (expression instanceof UnaryExpression) {
			visit((UnaryExpression) expression);
		} else if (expression instanceof AuxiliaryNameExpression) {
			visit((AuxiliaryNameExpression) expression);
		} else {
			throw new RuntimeException("Visited expression has unacceptable type");
		}
	}

	private void visit(TerminalEspression<?> expression) {
		AbstractQueryResult result = null;
		if (expression instanceof IntegerTerminal) {
			IntegerTerminal integerExpression = (IntegerTerminal) expression;
			Integer expressionValue = integerExpression.getValue();
			result = new IntegerResult(expressionValue);
		} else if (expression instanceof BooleanTerminal) {
			BooleanTerminal booleanExpression = (BooleanTerminal) expression;
			Boolean expressionValue = booleanExpression.getValue();
			result = new BooleanResult(expressionValue);
		} else if (expression instanceof DoubleTerminal) {
			DoubleTerminal doubleExpression = (DoubleTerminal) expression;
			Double expressionValue = doubleExpression.getValue();
			result = new DoubleResult(expressionValue);
		} else if (expression instanceof StringTerminal) {
			StringTerminal stringExpression = (StringTerminal) expression;
			String expressionValue = stringExpression.getValue();
			result = new StringResult(expressionValue);
		} else if (expression instanceof NameTerminal) {
			visit((NameTerminal) expression);
			return;
		} else {
			throw new RuntimeException("Terminal expression has unacceptable type");
		}

		qres.push(result);
	}

	private void visit(BinaryExpression expression) {
		if (expression instanceof EqualExpression) {
			// 1 == 3
			visit((EqualExpression) expression);
		} else if (expression instanceof NotEqualExpression) {
			// 3 != 4
			visit((NotEqualExpression) expression);
		} else if (expression instanceof GreaterExpression) {
			// 3.23 > 2
			visit((GreaterExpression) expression);
		} else if (expression instanceof GreaterOrEqualExpression) {
			// 3.0 >= 3
			visit((GreaterOrEqualExpression) expression);
		} else if (expression instanceof LesserExpression) {
			// 3 < 12
			visit((LesserExpression) expression);
		} else if (expression instanceof LesserOrEqualExpression) {
			// 43 <= 42
			visit((LesserOrEqualExpression) expression);
		} else if (expression instanceof AddExpression) {
			// 43 + 42
			visit((AddExpression) expression);
		} else if (expression instanceof SubtractExpression) {
			// 43 - 42
			visit((SubtractExpression) expression);
		} else if (expression instanceof MultiplyExpression) {
			// 43 * 42
			visit((MultiplyExpression) expression);
		} else if (expression instanceof DivideExpression) {
			// 43 / 42
			visit((DivideExpression) expression);
		} else if (expression instanceof ModuloExpression) {
			// 43 % 42
			visit((ModuloExpression) expression);
		} else if (expression instanceof AndExpression) {
			// 43 and 42
			visit((AndExpression) expression);
		} else if (expression instanceof OrExpression) {
			// 43 or 42
			visit((OrExpression) expression);
		} else if (expression instanceof NandExpression) {
			// 43 nand 42
			visit((NandExpression) expression);
		} else if (expression instanceof XorExpression) {
			// 43 xor 42
			visit((XorExpression) expression);
		} else if (expression instanceof ComaExpression) {
			// 1, 2, 3, 4
			visit((ComaExpression) expression);
		} else if (expression instanceof IntersectExpression) {
			// bag(1, 2, 3, 4) intersect bag(3, 4)
			visit((IntersectExpression) expression);
		} else if (expression instanceof ForallExpression) {
			// bag(true, true, true)
			visit((ForallExpression) expression);
		} else if (expression instanceof ForanyExpression) {
			// forall bag(1, 2, 3) as n (n % 2 = 0)
			visit((ForanyExpression) expression);
		} else if (expression instanceof MinusExpression) {
			// bag(1, 2, 3) minus bag(2)
			visit((MinusExpression) expression);
		} else if (expression instanceof UnionExpression) {
			// bag(1, 2, 3) union bag(2)
			visit((UnionExpression) expression);
		} else if (expression instanceof InExpression) {
			// bag(1, 2) in bag (1, 4, 2)
			visit((InExpression) expression);
		} else if (expression instanceof WhereExpression) {
			// bag(1, 2, 3) where true
			visit((WhereExpression) expression);
		} else if (expression instanceof JoinExpression) {
			// bag(1, 2, 3) join true
			visit((JoinExpression) expression);
		} else if (expression instanceof DotExpression) {
			// a.b
			visit((DotExpression) expression);
		} else {
			throw new RuntimeException("BinaryExpression " + expression.getClass().getSimpleName()
					+ " has not been implemented yet");
		}
	}

	private void visit(UnaryExpression expression) {
		if (expression instanceof BagExpression) {
			// bag(1, 2, 3)
			visit((BagExpression) expression);
		} else if (expression instanceof StructExpression) {
			// struct(1, 2, 4)
			visit((StructExpression) expression);
		} else if (expression instanceof MinExpression) {
			// min(1, 3, 4)
			visit((MinExpression) expression);
		} else if (expression instanceof MaxExpression) {
			// max(3, 4, 1, 5)
			visit((MaxExpression) expression);
		} else if (expression instanceof SumExpression) {
			// sum(3, 2, 5)
			visit((SumExpression) expression);
		} else if (expression instanceof AvgExpression) {
			// avg(3, 5, 6)
			visit((AvgExpression) expression);
		} else if (expression instanceof CountExpression) {
			// count(3, 5, 6)
			visit((CountExpression) expression);
		} else if (expression instanceof NotExpression) {
			// not true
			visit((NotExpression) expression);
		} else if (expression instanceof ExistsExpression) {
			// exists()
			visit((ExistsExpression) expression);
		} else if (expression instanceof UniqueExpression) {
			// unique(bag(1, 2, 1)
			visit((UniqueExpression) expression);
		} else if (expression instanceof EmptyExpression) {
			// empty(bag(1, 3, 2) as n where n % 2 == 0)
			visit((EmptyExpression) expression);
		} else if (expression instanceof UnaryMinusExpression) {
			// -3
			visit((UnaryMinusExpression) expression);
		} else if (expression instanceof UnaryPlusExpression) {
			// +6.3
			visit((UnaryPlusExpression) expression);
		}
	}

	private void visit(AuxiliaryNameExpression expression) {
		if (expression instanceof AsExpression) {
			visit((AsExpression) expression);
		} else if (expression instanceof GroupAsExpression) {
			visit((GroupAsExpression) expression);
		}
	}

	/**
	 * Operator nazwy
	 * 
	 * @param expression
	 */
	private void visit(NameTerminal expression) {
		String name = expression.getName();
		BagResult eres = envs.bind(name);
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator . (kropka)
	 * 
	 * @param expression
	 */
	private void visit(DotExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj lewe podzapytanie.
		Expression leftExpression = expression.getLeftExpression();
		visit(leftExpression);

		// 3. Podnieś jego rezultat z QRES i potraktuj jak baga
		AbstractQueryResult leftExpressionResult = qres.pop();
		BagResult q1 = QResUtils.convertToBag(leftExpressionResult);

		// 4. Dla każdego elementu el należącego do podniesionego wyniku wykonaj:
		for (AbstractQueryResult el : q1.getElements()) {
			// 1)Otwórz nową sekcję na ENVS.
			ENVSFrame envsFrame = new ENVSFrame();
			envs.push(envsFrame);

			// 2)Wykonaj operację nested(el)
			List<ENVSBinder> elBinders = envs.nested(el, sbaStore);
			envsFrame.add(elBinders);

			// 3)Wykonaj prawe podzapytanie.
			Expression rightExpression = expression.getRightExpression();
			visit(rightExpression);

			// 4)Podnieś jego rezultat z QRES.
			AbstractQueryResult q2res = qres.pop();
			// Dodaj rezultat (lub jego elementy) do eres
			BagResult q2resBag = QResUtils.convertToBag(q2res);
			eres.addElements(q2resBag.getElements());

			// Zamykamy ramkę envs
			envs.pop();
		}

		// 5. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator where
	 * 
	 * @param expression
	 */
	private void visit(WhereExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj lewe podzapytanie.
		Expression leftExpression = expression.getLeftExpression();
		visit(leftExpression);

		// 3. Podnieś jego rezultat z QRES i potraktuj jak baga
		AbstractQueryResult leftExpressionResult = qres.pop();
		BagResult q1 = QResUtils.convertToBag(leftExpressionResult);

		// 4. Dla każdego elementu el należącego do podniesionego wyniku wykonaj:
		for (AbstractQueryResult el : q1.getElements()) {
			// 1)Otwórz nową sekcję na ENVS.
			ENVSFrame envsFrame = new ENVSFrame();
			envs.push(envsFrame);

			// 2)Wykonaj operację nested(el)
			List<ENVSBinder> elBinders = envs.nested(el, sbaStore);
			envsFrame.add(elBinders);

			// 3)Wykonaj prawe podzapytanie.
			Expression rightExpression = expression.getRightExpression();
			visit(rightExpression);

			// 4)Podnieś jego rezultat z QRES.
			AbstractQueryResult q2res = qres.pop();

			// Dokonaj ewentualnej dereferencji
			dereferenceIfNecessary(q2res);

			q2res = qres.pop();

			if (!(q2res instanceof BooleanResult)) {
				throw new RuntimeException("Result of right expression for element " + el.toString()
						+ " is not a BooleanResult");
			}

			BooleanResult rightExpressionBooleanResult = (BooleanResult) q2res;

			if (rightExpressionBooleanResult.getValue()) {
				eres.addElement(el);
			}

			// Zamykamy ramkę envs
			envs.pop();
		}

		// 5. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator join
	 * 
	 * @param expression
	 */
	private void visit(JoinExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj lewe podzapytanie.
		Expression leftExpression = expression.getLeftExpression();
		visit(leftExpression);

		// 3. Podnieś jego rezultat z QRES i potraktuj jak baga
		AbstractQueryResult leftExpressionResult = qres.pop();
		BagResult q1 = QResUtils.convertToBag(leftExpressionResult);

		// 4. Dla każdego elementu el należącego do podniesionego wyniku wykonaj:
		for (AbstractQueryResult el1 : q1.getElements()) {
			if (el1 instanceof StructResult) {
				continue;
			}

			// 1)Otwórz nową sekcję na ENVS.
			ENVSFrame envsFrame = new ENVSFrame();
			envs.push(envsFrame);

			// 2)Wykonaj operację nested(el)
			List<ENVSBinder> elBinders = envs.nested(el1, sbaStore);
			envsFrame.add(elBinders);

			// 3)Wykonaj prawe podzapytanie.
			Expression rightExpression = expression.getRightExpression();
			visit(rightExpression);

			// 4)Podnieś jego rezultat z QRES.
			AbstractQueryResult el2 = qres.pop();

			// Zrób z tego baga
			BagResult el2Bag = QResUtils.convertToBag(el2);

			// I Utwórz strukturę { el1, el2 }. Jeśli el2 to struktura/y należy wziąć ich elementy – analogicznie jak w
			// przypadku operatora , (przecinka). el1 nie może być strukturą.
			for (AbstractQueryResult el2Element : el2Bag.getElements()) {
				StructResult el1El2Structure = StructResult.createStructure(el1, el2Element);
				eres.addElement(el1El2Structure);
			}

			// Zamykamy ramkę envs
			envs.pop();
		}

		// 5. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator =
	 * 
	 * @param expression
	 */
	private void visit(EqualExpression expression) {
		// equals operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 == number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 == number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 == number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 == number2);
			}

			// Operations with Strings

			public AbstractQueryResult performOperation(int number1, String string) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(double number1, String string) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(String string, int number) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(String string, double number) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(String string1, String string2) {
				return new BooleanResult(StringUtils.equals(string1, string2));
			}

			// Operations with Booleans

			public AbstractQueryResult performOperation(int number, boolean bool) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(double number, boolean bool) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(String string, boolean bool) {
				return new BooleanResult(false);
			}

			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				return new BooleanResult(bool1 == bool2);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator <>
	 * 
	 * @param expression
	 */
	private void visit(NotEqualExpression expression) {
		// not equals operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 != number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 != number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 != number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 != number2);
			}

			// Operations with Strings

			public AbstractQueryResult performOperation(int number1, String string) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(double number1, String string) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(String string, int number) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(String string, double number) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(String string1, String string2) {
				return new BooleanResult(!StringUtils.equals(string1, string2));
			}

			// Operations with Booleans

			public AbstractQueryResult performOperation(int number, boolean bool) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(double number, boolean bool) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(String string, boolean bool) {
				return new BooleanResult(true);
			}

			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				return new BooleanResult(true);
			}

		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator przecinka (,)
	 * 
	 * @param expression
	 */
	private void visit(ComaExpression expression) {
		AbstractQueryResult eres = null;

		// 2. Wykonaj oba podwyrażenia.
		Expression leftExpression = expression.getLeftExpression();
		Expression righExpression = expression.getRightExpression();
		visit(leftExpression);
		visit(righExpression);

		// 3. Podnieś jeden element z QRES (nazywany dalej e2res).
		// Traktujemy elementy jak strukturę
		AbstractQueryResult e2res = qres.pop();
		// 4. Podnieś drugi element z QRES (nazywany dalej e1res).
		AbstractQueryResult e1res = qres.pop();

		boolean bothElementsAreSingleResults = e2res instanceof SingleResult && e1res instanceof SingleResult;
		if (bothElementsAreSingleResults) {
			eres = StructResult.createStructure(e1res, e2res);
		} else {
			// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
			eres = new BagResult();
			BagResult eresBag = (BagResult) eres;

			BagResult e2resStructure = QResUtils.convertToBag(e2res);
			BagResult e1resStructure = QResUtils.convertToBag(e1res);

			// 5. Dla każdego elementu e1 z e1res wykonaj:
			for (AbstractQueryResult e1 : e1resStructure.getElements()) {
				// 1) Dla każdego elementu e2 z e2res wykonaj:
				for (AbstractQueryResult e2 : e2resStructure.getElements()) {
					// I. Utwórz strukturę { e1, e2 }.
					StructResult e1e2Structure = StructResult.createStructure(e1, e2);
					// II. Dodaj nowo stworzoną strukturę do eres.
					eresBag.addElement(e1e2Structure);
				}
			}

			// Jeśli eres zawiera tylko jedną strukturę, to ją "wypakuj"
			if (eresBag.getElements().size() == 1) {
				eresBag = QResUtils.unpackStructFromBag(eresBag);
			}
		}

		// 6. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator >
	 * 
	 * @param expression
	 */
	private void visit(GreaterExpression expression) {
		// greater operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 > number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 > number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 > number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 > number2);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator >=
	 * 
	 * @param expression
	 */
	private void visit(GreaterOrEqualExpression expression) {
		// greater or equal operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 >= number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 >= number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 >= number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 >= number2);
			}

		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator <
	 * 
	 * @param expression
	 */
	private void visit(LesserExpression expression) {
		// equals operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 < number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 < number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 < number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 < number2);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator <=
	 * 
	 * @param expression
	 */
	private void visit(LesserOrEqualExpression expression) {
		// equals operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				return new BooleanResult(number1 <= number2);
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				return new BooleanResult(number1 <= number2);
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				return new BooleanResult(number1 <= number2);
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				return new BooleanResult(number1 <= number2);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator +
	 * 
	 * @param expression
	 */
	private void visit(AddExpression expression) {
		// add operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				double result = (double) number1 + (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				double result = (double) number1 + (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				double result = (double) number1 + (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				double result = (double) number1 + (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number, String string) {
				return new StringResult(number + string);
			}

			public AbstractQueryResult performOperation(double number, String string) {
				return new StringResult(number + string);
			}

			public AbstractQueryResult performOperation(boolean bool, String string) {
				return new StringResult(bool + string);
			}

			public AbstractQueryResult performOperation(String string, int number) {
				return new StringResult(string + number);
			}

			public AbstractQueryResult performOperation(String string, double number) {
				return new StringResult(string + number);
			}

			public AbstractQueryResult performOperation(String string, boolean bool) {
				return new StringResult(string + bool);
			}

			public AbstractQueryResult performOperation(String string1, String string2) {
				return new StringResult(string1 + string2);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator -
	 * 
	 * @param expression
	 */
	private void visit(SubtractExpression expression) {
		// subtract operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				double result = (double) number1 - (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				double result = (double) number1 - (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				double result = (double) number1 - (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				double result = (double) number1 - (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator *
	 * 
	 * @param expression
	 */
	private void visit(MultiplyExpression expression) {
		// multiply operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				double result = (double) number1 * (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				double result = (double) number1 * (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				double result = (double) number1 * (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				double result = (double) number1 * (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator /
	 * 
	 * @param expression
	 */
	private void visit(DivideExpression expression) {
		// divide operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				double result = (double) number1 / (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				double result = (double) number1 / (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				double result = (double) number1 / (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				double result = (double) number1 / (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator %
	 * 
	 * @param expression
	 */
	private void visit(ModuloExpression expression) {
		// modulo operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(int number1, int number2) {
				double result = (double) number1 % (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(int number1, double number2) {
				double result = (double) number1 % (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, int number2) {
				double result = (double) number1 % (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}

			public AbstractQueryResult performOperation(double number1, double number2) {
				double result = (double) number1 % (double) number2;
				if (result % 1 != 0) {
					return new DoubleResult(result);
				} else {
					return new IntegerResult((int) result);
				}
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator and
	 * 
	 * @param expression
	 */
	private void visit(AndExpression expression) {
		// and operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				boolean result = bool1 && bool2;
				return new BooleanResult(result);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator or
	 * 
	 * @param expression
	 */
	private void visit(OrExpression expression) {
		// and operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				boolean result = bool1 || bool2;
				return new BooleanResult(result);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator xor
	 * 
	 * @param expression
	 */
	private void visit(XorExpression expression) {
		// and operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				boolean result = bool1 ^ bool2;
				return new BooleanResult(result);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator nand
	 * 
	 * @param expression
	 */
	private void visit(NandExpression expression) {
		// and operator
		ArithmeticOperator arithmeticOperator = new ArithmeticOperator() {
			public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
				boolean result = !(bool1 && bool2);
				return new BooleanResult(result);
			}
		};

		performArithmeticExpression(expression, arithmeticOperator);
	}

	/**
	 * Operator minus
	 * 
	 * @param expression
	 */
	private void visit(MinusExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj oba podwyrażenia.
		Expression leftExpression = expression.getLeftExpression();
		Expression righExpression = expression.getRightExpression();
		visit(leftExpression);
		visit(righExpression);

		// 3. Podnieś dwa elementy z QRES.
		AbstractQueryResult rightResult = qres.pop();
		AbstractQueryResult leftResult = qres.pop();
		// Jeśli to pojedyncze elementy, to udaj, ze to bagi z jednym elementem
		CollectionResult e2res = QResUtils.convertToBag(rightResult);
		CollectionResult e1res = QResUtils.convertToBag(leftResult);

		// 4. Dla każdego elementu z e1res
		for (AbstractQueryResult element : e1res.getElements()) {
			// 5. Sprawdź czy e2res go zawiera
			if (!e2res.contains(element)) {
				eres.addElement(element);
			}
		}

		// 6. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator intersect
	 * 
	 * @param expression
	 */
	private void visit(IntersectExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj oba podwyrażenia.
		Expression leftExpression = expression.getLeftExpression();
		Expression righExpression = expression.getRightExpression();
		visit(leftExpression);
		visit(righExpression);

		// 3. Podnieś dwa elementy z QRES.
		AbstractQueryResult rightResult = qres.pop();
		AbstractQueryResult leftResult = qres.pop();
		// Jeśli to pojedyncze elementy, to udaj, ze to bagi z jednym elementem
		CollectionResult e2res = QResUtils.convertToBag(rightResult);
		CollectionResult e1res = QResUtils.convertToBag(leftResult);

		// 4. Dla każdego elementu z e1res
		for (AbstractQueryResult element : e1res.getElements()) {
			// 5. Sprawdź czy e2res go zawiera. Jeśli tak, to go dodaj do eres
			if (e2res.contains(element)) {
				eres.addElement(element);
			}
		}

		// 6. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator union
	 * 
	 * @param expression
	 */
	private void visit(UnionExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj oba podwyrażenia.
		Expression leftExpression = expression.getLeftExpression();
		Expression righExpression = expression.getRightExpression();
		visit(leftExpression);
		visit(righExpression);

		// 3. Podnieś dwa elementy z QRES.
		AbstractQueryResult rightResult = qres.pop();
		AbstractQueryResult leftResult = qres.pop();
		// Jeśli to pojedyncze elementy, to udaj, ze to bagi z jednym elementem
		CollectionResult e2res = QResUtils.convertToBag(rightResult);
		CollectionResult e1res = QResUtils.convertToBag(leftResult);

		// 4. Każdy element z e1res dodaj do eres
		for (AbstractQueryResult element : e1res.getElements()) {
			eres.addElement(element);
		}
		// 5. Każdy element z e1res dodaj do eres
		for (AbstractQueryResult element : e2res.getElements()) {
			eres.addElement(element);
		}

		// 6. Dodaj eres do QRES.
		if (eres.canBeConvertedToSingleResult()) {
			SingleResult eresAsSingleResult = eres.convertToSingleResult();
			qres.push(eresAsSingleResult);
		} else {
			qres.push(eres);
		}
	}

	/**
	 * Operator in
	 * 
	 * @param expression
	 */
	private void visit(InExpression expression) {
		// 1. Zainicjalizuj zmienną z odpowiedzią
		boolean eresBoolean = true;

		// 2. Wykonaj oba podwyrażenia.
		Expression leftExpression = expression.getLeftExpression();
		Expression righExpression = expression.getRightExpression();
		visit(leftExpression);
		visit(righExpression);

		// 3. Podnieś dwa elementy z QRES.
		AbstractQueryResult e2res = qres.pop();
		AbstractQueryResult e1res = qres.pop();
		StructResult e2resCollection = e2res.toStruct();
		StructResult e1resCollection = e1res.toStruct();

		// 4. Dla każdego elementu z e2res
		for (AbstractQueryResult element : e1resCollection.getElements()) {
			if (!e2resCollection.contains(element)) {
				eresBoolean = false;
				break;
			}
		}

		// 5. Dodaj eres do QRES.
		BooleanResult eres = new BooleanResult(eresBoolean);
		qres.push(eres);
	}

	/**
	 * Operator exists
	 * 
	 * @param expression
	 */
	private void visit(ExistsExpression expression) {
		// 1. Zainicjalizuj zmienną z odpowiedzią
		Boolean eresBoolean = false;

		// 2. Wykonaj podzapytanie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś rezultat z QRES.
		CollectionResult expressionResult = QResUtils.convertToBag(qres.pop());

		if (expressionResult.getElements().size() > 0) {
			eresBoolean = true;
		}

		BooleanResult eres = new BooleanResult(eresBoolean);

		// 3. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator unique
	 * 
	 * @param expression
	 */
	private void visit(UniqueExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj podwyrażenie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// 4. Potraktuj go jak strukturę; Każdy element tej struktury dodaj do eres uważając na to by się nie powtarzał.
		StructResult struct = innerResult.toStruct();

		List<AbstractQueryResult> structElements = struct.getElements();

		for (AbstractQueryResult structElement : structElements) {
			if (!eres.contains(structElement)) {
				eres.addElement(structElement);
			}
		}

		// 5. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator empty
	 * 
	 * @param expression
	 */
	private void visit(EmptyExpression expression) {
		// 1. Zainicjalizuj zmienną z odpowiedzią
		Boolean eresBoolean = false;

		// 2. Wykonaj podzapytanie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś rezultat z QRES.
		AbstractQueryResult expressionResult = qres.pop();

		if (expressionResult instanceof CollectionResult) {
			CollectionResult collectionResult = (CollectionResult) expressionResult;

			if (collectionResult.getElements().size() == 0) {
				eresBoolean = true;
			}
		} else {
			eresBoolean = false;
		}

		BooleanResult eres = new BooleanResult(eresBoolean);

		// 4. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator forall
	 * 
	 * @param expression
	 */
	private void visit(ForallExpression expression) {
		// 1. Zainicjalizuj zmienną z odpowiedzią
		Boolean eresBoolean = true;

		// 1. Wykonaj lewe podzapytanie.
		Expression leftExpression = expression.getLeftExpression();
		visit(leftExpression);

		// 2. Podnieś jego rezultat z QRES.
		AbstractQueryResult e1res = qres.pop();
		// potraktuj to jako baga
		e1res = QResUtils.convertToBag(e1res);

		// 3. Dla każdego elementu el należącego do podniesionego wyniku wykonaj:
		for (AbstractQueryResult element : ((CollectionResult) e1res).getElements()) {
			// 1)Otwórz nową sekcję na ENVS.
			ENVSFrame envsFrame = new ENVSFrame();
			envs.push(envsFrame);

			// 2)Wykonaj operację nested(el).
			List<ENVSBinder> binders = envs.nested(element, sbaStore);
			envsFrame.add(binders);

			// 3)Wykonaj prawe podzapytanie.
			Expression rightExpression = expression.getRightExpression();
			visit(rightExpression);

			// 4)Podnieś jego rezultat z QRES.
			AbstractQueryResult e2res = qres.pop();
			// Jeśli trzeba, to dokonaj dereferencji
			dereferenceIfNecessary(e2res);
			e2res = qres.pop();

			if (e2res instanceof BooleanResult) {
				BooleanResult e2resBoolean = (BooleanResult) e2res;
				if (!e2resBoolean.getValue()) {
					eresBoolean = false;
					break;
				}
			} else {
				throw new RuntimeException(e2res + " is not a BooleanResult");
			}
		}

		BooleanResult eres = new BooleanResult(eresBoolean);

		// 5. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator forany
	 * 
	 * @param expression
	 */
	private void visit(ForanyExpression expression) {
		// 1. Zainicjalizuj zmienną z odpowiedzią
		Boolean eresBoolean = false;

		// 1. Wykonaj lewe podzapytanie.
		Expression leftExpression = expression.getLeftExpression();
		visit(leftExpression);

		// 2. Podnieś jego rezultat z QRES.
		AbstractQueryResult e1res = qres.pop();
		// potraktuj to jako baga
		e1res = QResUtils.convertToBag(e1res);

		// 3. Dla każdego elementu el należącego do podniesionego wyniku wykonaj:
		for (AbstractQueryResult element : ((CollectionResult) e1res).getElements()) {
			// 1)Otwórz nową sekcję na ENVS.
			ENVSFrame envsFrame = new ENVSFrame();
			envs.push(envsFrame);

			// 2)Wykonaj operację nested(el).
			List<ENVSBinder> binders = envs.nested(element, sbaStore);
			envsFrame.add(binders);

			// 3)Wykonaj prawe podzapytanie.
			Expression rightExpression = expression.getRightExpression();
			visit(rightExpression);

			// 4)Podnieś jego rezultat z QRES.
			AbstractQueryResult e2res = qres.pop();
			// Jeśli trzeba, to dokonaj dereferencji
			dereferenceIfNecessary(e2res);
			e2res = qres.pop();

			if (e2res instanceof BooleanResult) {
				BooleanResult e2resBoolean = (BooleanResult) e2res;
				if (e2resBoolean.getValue()) {
					eresBoolean = true;
					break;
				}
			} else {
				throw new RuntimeException(e2res + " is not a BooleanResult");
			}
		}

		BooleanResult eres = new BooleanResult(eresBoolean);

		// 5. Dodaj eres do QRES.
		qres.push(eres);
	}

	private void visit(BagExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		BagResult eres = new BagResult();

		// 2. Wykonaj podwyrażenie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// 4. Potraktuj go jak strukturę; Każdy element tej struktury dodaj do eres.
		StructResult struct = innerResult.toStruct();
		eres.addElements(struct.getElements());

		// 5. Dodaj eres do QRES.
		qres.push(eres);
	}

	private void visit(StructExpression expression) {
		// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
		StructResult eres = new StructResult();

		// 2. Wykonaj podwyrażenie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// 4. Potraktuj go jak strukturę; Każdy element tej struktury dodaj do eres.
		StructResult struct = innerResult.toStruct();
		eres.addElements(struct.getElements());

		// 5. Dodaj eres do QRES.
		qres.push(eres);
	}

	/**
	 * Operator min
	 * 
	 * @param expression
	 */
	private void visit(MinExpression expression) {
		AggregationOperatorPerformer expressionOperator = new AggregationOperatorPerformer() {
			double min = Double.MAX_VALUE;

			@Override
			public void addElement(Integer element) {
				if (element < min) {
					min = element;
				}
			}

			@Override
			public void addElement(Double element) {
				if (element < min) {
					min = element;
				}
			}

			@Override
			public AbstractQueryResult getResult() {
				if (min % 1.0 == 0) {
					return new IntegerResult((int) min);
				} else {
					return new DoubleResult(min);
				}
			}
		};

		performAggregationExpression(expression, expressionOperator);
	}

	/**
	 * Operator max
	 * 
	 * @param expression
	 */
	private void visit(MaxExpression expression) {
		AggregationOperatorPerformer expressionOperator = new AggregationOperatorPerformer() {
			double max = -Double.MAX_VALUE;

			@Override
			public void addElement(Integer element) {
				if (element > max) {
					max = element;
				}
			}

			@Override
			public void addElement(Double element) {
				if (element > max) {
					max = element;
				}
			}

			@Override
			public AbstractQueryResult getResult() {
				if (max % 1.0 == 0) {
					return new IntegerResult((int) max);
				} else {
					return new DoubleResult(max);
				}
			}
		};

		performAggregationExpression(expression, expressionOperator);
	}

	/**
	 * Operator sum
	 * 
	 * @param expression
	 */
	private void visit(SumExpression expression) {
		AggregationOperatorPerformer expressionOperator = new AggregationOperatorPerformer() {
			double sum = 0.0;

			@Override
			public void addElement(Integer element) {
				sum += element;
			}

			@Override
			public void addElement(Double element) {
				sum += element;
			}

			@Override
			public AbstractQueryResult getResult() {
				if (sum % 1.0 == 0) {
					return new IntegerResult((int) sum);
				} else {
					return new DoubleResult(sum);
				}
			}
		};

		performAggregationExpression(expression, expressionOperator);
	}

	private void performAggregationExpression(UnaryExpression expression, AggregationOperatorPerformer eres) {
		// 1. Zainicjalizuj wartość, która będzie służyć do przechowywania sumy (nazywamy dalej eres).
		// Ten krok pomijamy, bo jest implementowany przez AggregationOperatorPerformer

		// 2. Wykonaj podzapytanie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 3. Podnieś wynik zapytania z QRES.
		AbstractQueryResult resultOfInnerExpression = qres.pop();
		Collection<AbstractQueryResult> collection = retrieveElementsFromResult(resultOfInnerExpression);

		// 4. Dla każdego elementu el należącego do w/w wyniku zapytania wykonaj:
		for (AbstractQueryResult element : collection) {
			// Dla referencji wykonaj najpierw dereferencję
			dereferenceIfNecessary(element);
			AbstractQueryResult elementResult = (AbstractQueryResult) qres.pop();

			if (elementResult instanceof SimpleResult<?>) {
				Object elementResultObject = ((SimpleResult<?>) elementResult).getValue();

				if (elementResultObject instanceof Integer) {
					eres.addElement((Integer) elementResultObject);
				} else if (elementResultObject instanceof Double) {
					eres.addElement((Double) elementResultObject);
				}
			} else {
				eres.addElement(element);
			}
		}

		AbstractQueryResult result = eres.getResult();

		// 5. Dodaj eres do QRES.
		qres.push(result);
	}

	// Buduje kolekcję elementów na podstawie rezultatu z qres
	private Collection<AbstractQueryResult> retrieveElementsFromResult(AbstractQueryResult result) {
		Collection<AbstractQueryResult> output = null;

		if (result instanceof CollectionResult) {
			CollectionResult collection = (CollectionResult) result;
			output = collection.getElements();
		} else if (result instanceof SimpleResult<?>) {
			output = new ArrayList<AbstractQueryResult>(1);
			output.add(result);
		} else if (result instanceof StructResult) {
			output = new ArrayList<AbstractQueryResult>();
			output.add(result);
		} else {
			throw new RuntimeException("It's impossible to retrieve elements from " + result.getClass().getSimpleName());
		}

		return output;
	}

	/**
	 * Operator avg
	 * 
	 * @param expression
	 */
	private void visit(AvgExpression expression) {
		AggregationOperatorPerformer expressionOperator = new AggregationOperatorPerformer() {
			double sum = 0.0;
			int numberOfElements = 0;

			@Override
			public void addElement(Integer element) {
				sum += element;
				numberOfElements++;
			}

			@Override
			public void addElement(Double element) {
				sum += element;
				numberOfElements++;
			}

			@Override
			public AbstractQueryResult getResult() {
				double average = sum / numberOfElements;
				if (average % 1.0 == 0) {
					return new IntegerResult((int) average);
				} else {
					return new DoubleResult(average);
				}
			}
		};

		performAggregationExpression(expression, expressionOperator);
	}

	/**
	 * Operator count
	 * 
	 * @param expression
	 */
	private void visit(CountExpression expression) {
		AggregationOperatorPerformer expressionOperator = new AggregationOperatorPerformer() {
			int numberOfElements = 0;

			@Override
			public void addElement(Integer element) {
				numberOfElements++;
			}

			@Override
			public void addElement(Double element) {
				numberOfElements++;
			}

			@Override
			public void addElement(Object element) {
				numberOfElements++;
			}

			@Override
			public AbstractQueryResult getResult() {
				return new IntegerResult(numberOfElements);
			}
		};

		performAggregationExpression(expression, expressionOperator);
	}

	/**
	 * Operator not
	 * 
	 * @param expression
	 */
	private void visit(NotExpression expression) {
		// Wykonaj podwyrażenie
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// Jeśli wyrażenie jest bagiem z jednym elementem, to go spróbuj wypakować
		if (innerResult instanceof BagResult) {
			BagResult innerResultBag = (BagResult) innerResult;
			if (innerResultBag.canBeConvertedToSingleResult()) {
				innerResult = innerResultBag.convertToSingleResult();
			}
		}

		// Jeśli wyrażenie jest referencją, dokonaj dereferencji
		if (innerResult instanceof ReferenceResult) {
			dereference((ReferenceResult) innerResult);
			innerResult = qres.pop();
		}

		// Upewnij się, że rezultat jest właściwego typu
		BooleanResult innerResultAsBoolean = null;
		if (innerResult instanceof BooleanResult) {
			innerResultAsBoolean = (BooleanResult) innerResult;
		} else {
			throw new RuntimeException("Not can be performed only on boolean results");
		}

		// Dokonaj negacji i wrzuć rezultat na qres
		boolean expressionValue = innerResultAsBoolean.getValue();

		BooleanResult negatedResult = new BooleanResult(!expressionValue);

		qres.push(negatedResult);
	}

	/**
	 * Operator -
	 * 
	 * @param expression
	 */
	private void visit(UnaryMinusExpression expression) {
		// Wykonaj podwyrażenie
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// Jeśli wyrażenie jest referencją, dokonaj dereferencji
		dereferenceIfNecessary(innerResult);
		innerResult = qres.pop();

		AbstractQueryResult result = null;
		// Wykonaj operację -
		if (innerResult instanceof IntegerResult) {
			IntegerResult integerResult = (IntegerResult) innerResult;
			result = new IntegerResult(-integerResult.getValue());
		} else if (innerResult instanceof DoubleResult) {
			DoubleResult doubleResult = (DoubleResult) innerResult;
			result = new DoubleResult(-doubleResult.getValue());
		} else {
			throw new RuntimeException(
					"Unary minus operator can be used only on Integers or Doubles. The actual type is "
							+ innerResult.getClass().getSimpleName());
		}

		qres.push(result);
	}

	/**
	 * Operator +
	 * 
	 * @param expression
	 */
	private void visit(UnaryPlusExpression expression) {
		// Wykonaj podwyrażenie
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// Podnieś wynik z QRES.
		AbstractQueryResult innerResult = qres.pop();

		// Jeśli wyrażenie jest referencją, dokonaj dereferencji
		dereferenceIfNecessary(innerResult);
		innerResult = qres.pop();

		// Upewnij się że podwyrażenie zwróciło właściwy typ
		if (!(innerResult instanceof IntegerResult) && !(innerResult instanceof DoubleResult)) {
			throw new RuntimeException(
					"Unary plus operator can be used only on Integers or Doubles. The actual type is "
							+ innerResult.getClass().getSimpleName());
		}

		qres.push(innerResult);
	}

	/**
	 * Operator as
	 * 
	 * @param expression
	 */
	private void visit(AsExpression expression) {
		String binderName = expression.getAuxiliaryName();

		// 1. Wykonaj podzapytanie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 2. Podnieś rezultat z QRES.
		AbstractQueryResult expressionResult = (AbstractQueryResult) qres.pop();
		// Zamieniamy rezultat na baga
		BagResult expressionResultAsBag = QResUtils.convertToBag(expressionResult);

		// 3. Każdy element otrzymanej kolekcji zastąp binderem o nazwie podanej jako parametr operatora i wartości
		// będącej zastępowanym rezultatem.
		BagResult result = new BagResult();
		for (AbstractQueryResult element : expressionResultAsBag.getElements()) {
			Binder binder = new Binder(binderName, element);
			result.addElement(binder);
		}

		// 4. Dodaj wynik na QRES.
		if (result.canBeConvertedToSingleResult()) {
			qres.push(result.convertToSingleResult());
		} else {
			qres.push(result);
		}
	}

	/**
	 * Operator groupas
	 * 
	 * @param expression
	 */
	private void visit(GroupAsExpression expression) {
		String binderName = expression.getAuxiliaryName();

		// 1. Wykonaj podzapytanie.
		Expression innerExpression = expression.getInnerExpression();
		visit(innerExpression);

		// 2. Podnieś rezultat z QRES.
		AbstractQueryResult expressionResult = (AbstractQueryResult) qres.pop();

		// 3. Utwórz binder o nazwie podanej jako parametr operatora oraz wartości otrzymanej w poprzednim kroku.
		Binder binder = new Binder(binderName, expressionResult);

		// 4. Połóż go na QRES.
		qres.push(binder);
	}

	/**
	 * Wykonuje operację arytmetyczną
	 * 
	 * @param expression
	 * @param arithmeticOperator
	 */
	public void performArithmeticExpression(BinaryExpression expression, ArithmeticOperator arithmeticOperator) {
		performArithmeticExpression(expression.getLeftExpression(), expression.getRightExpression(), arithmeticOperator);
	}

	/**
	 * Wykonuje operację arytmetyczną.
	 * 
	 * @param expression
	 */
	public void performArithmeticExpression(Expression leftExpression, Expression rightExpression,
			ArithmeticOperator arithmeticOperator) {
		// 1. Wykonaj oba podzapytania.
		visit(leftExpression);
		visit(rightExpression);

		// 2. Podnieś oba elementy z QRES. Pamiętaj o kolejności.
		AbstractQueryResult q2res = qres.pop();
		AbstractQueryResult q1res = qres.pop();

		// Jeśli wartością jest bag z jednym elementem, którego można wypakować, to to zrób
		if (q2res instanceof BagResult) {
			BagResult q2resBag = (BagResult) q2res;
			if (q2resBag.canBeConvertedToSingleResult()) {
				q2res = q2resBag.convertToSingleResult();
			}
		}
		if (q1res instanceof BagResult) {
			BagResult q1resBag = (BagResult) q1res;
			if (q1resBag.canBeConvertedToSingleResult()) {
				q1res = q1resBag.convertToSingleResult();
			}
		}
		// Ewentualną referencję trzeba jeszcze poddać dereferencji
		if (q2res instanceof ReferenceResult) {
			dereference((ReferenceResult) q2res);
			q2res = qres.pop();
		}
		if (q1res instanceof ReferenceResult) {
			dereference((ReferenceResult) q1res);
			q1res = qres.pop();
		}

		// 3. Sprawdź czy to są pojedyncze wartości. Jeśli nie podnieś wyjątek.
		boolean q2resIsSimpleResult = q2res instanceof SingleResult;
		boolean q1resIsSimpleResult = q1res instanceof SingleResult;
		if (!q1resIsSimpleResult || !q2resIsSimpleResult) {
			throw new RuntimeException("Both expressions have to be SimpleResult type to perform correctly");
		}

		SimpleResult<?> q1resSimpleType = (SimpleResult<?>) q1res;
		SimpleResult<?> q2resSimpleType = (SimpleResult<?>) q2res;

		// 5. Sprawdź typy danych obu rezultatów. Jeśli nie są kompatybilne z operatorem, podnieś wyjątek.
		// 6. Wykonaj operację operatora.
		AbstractQueryResult result = arithmeticOperator.performProperOperation(q1resSimpleType, q2resSimpleType);

		// 7. Dodaj rezultat na QRES.
		qres.push(result);

	}

	/**
	 * Jeśli obiekt jest referencją, to poddaje go dereferencji i wrzuca rezultat na stos jeśli obiekt jest zwykłym
	 * obiektem to normalnie go wrzuca na stos
	 * 
	 * @param result
	 */
	private void dereferenceIfNecessary(AbstractQueryResult result) {
		if (result instanceof ReferenceResult) {
			dereference((ReferenceResult) result);
		} else {
			qres.push(result);
		}
	}

	/**
	 * Dokonuje dereferencji. Wskazywany obiekt jest umieszczany na stosie qres
	 * 
	 * @param reference
	 */
	private void dereference(ReferenceResult reference) {
		OID objectOid = reference.getOIDValue();
		SBAObject referencedObject = sbaStore.retrieve(objectOid);

		if (referencedObject instanceof SimpleObject<?>) {
			dereferenceSimpleObject((SimpleObject) referencedObject);
		} else if (referencedObject instanceof ComplexObject) {
			dereferenceComplexObject((ComplexObject) referencedObject);
		}
	}

	/**
	 * Dokonuje dereferencji referencji do typu złożonego
	 * 
	 * @param referencedObject
	 */
	private void dereferenceComplexObject(ComplexObject referencedObject) {
		throw new RuntimeException("Not implemented yet dereferenceComplexObject");
	}

	/**
	 * Dokonuje dereferencji referencji do typu prostego
	 * 
	 * @param referencedObject
	 */
	private void dereferenceSimpleObject(SimpleObject referencedObject) {
		Object objectValue = referencedObject.getValue();

		AbstractQueryResult result = null;
		if (objectValue instanceof Integer) {
			result = new IntegerResult((Integer) objectValue);
		} else if (objectValue instanceof Double) {
			result = new DoubleResult((Double) objectValue);
		} else if (objectValue instanceof String) {
			result = new StringResult((String) objectValue);
		} else if (objectValue instanceof Boolean) {
			result = new BooleanResult((Boolean) objectValue);
		} else {
			throw new RuntimeException("Dereference of SimpleObject of unsupported type");
		}

		qres.push(result);
	}
}
