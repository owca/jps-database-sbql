package info.owczarek.jps.database.ast.visitor.operationExecutors;

import info.owczarek.jps.database.qres.*;

public abstract class ArithmeticOperator {
	public final AbstractQueryResult performProperOperation(SimpleResult<?> result1, SimpleResult<?> result2) {
		Object result1Object = result1.getValue();
		Object result2Object = result2.getValue();

		if (result1Object instanceof Integer && result2Object instanceof Integer) {
			return performOperation((Integer) result1Object, (Integer) result2Object);
		} else if (result1Object instanceof Integer && result2Object instanceof Double) {
			return performOperation((Integer) result1Object, (Double) result2Object);
		} else if (result1Object instanceof Integer && result2Object instanceof String) {
			return performOperation((Integer) result1Object, (String) result2Object);
		} else if (result1Object instanceof Double && result2Object instanceof Integer) {
			return performOperation((Double) result1Object, (Integer) result2Object);
		} else if (result1Object instanceof Double && result2Object instanceof Double) {
			return performOperation((Double) result1Object, (Double) result2Object);
		} else if (result1Object instanceof Double && result2Object instanceof String) {
			return performOperation((Double) result1Object, (String) result2Object);
		} else if (result1Object instanceof String && result2Object instanceof Integer) {
			return performOperation((String) result1Object, (Integer) result2Object);
		} else if (result1Object instanceof String && result2Object instanceof Double) {
			return performOperation((String) result1Object, (Double) result2Object);
		} else if (result1Object instanceof String && result2Object instanceof String) {
			return performOperation((String) result1Object, (String) result2Object);
		} else if (result1Object instanceof Integer && result2Object instanceof Boolean) {
			return performOperation((Integer) result1Object, (Boolean) result2Object);
		} else if (result1Object instanceof Double && result2Object instanceof Boolean) {
			return performOperation((Double) result1Object, (Boolean) result2Object);
		} else if (result1Object instanceof String && result2Object instanceof Boolean) {
			return performOperation((String) result1Object, (Boolean) result2Object);
		} else if (result1Object instanceof Boolean && result2Object instanceof Boolean) {
			return performOperation((Boolean) result1Object, (Boolean) result2Object);
		} else if (result1Object instanceof Boolean && result2Object instanceof String) {
			return performOperation((Boolean) result1Object, (String) result2Object);
		} else if (result1Object instanceof Boolean && result2Object instanceof Double) {
			return performOperation((Boolean) result1Object, (Double) result2Object);
		} else if (result1Object instanceof Boolean && result2Object instanceof Integer) {
			return performOperation((Boolean) result1Object, (Integer) result2Object);
		} else {
			throw new RuntimeException("It's impossible to reach this code from ArithmeticOperator");
		}
	}

	// Operations on numbers

	public AbstractQueryResult performOperation(int number1, int number2) {
		throw new RuntimeException("Operation cannot be performed for two Integers");
	}

	public AbstractQueryResult performOperation(int number1, double number2) {
		throw new RuntimeException("Operation cannot be performed for Integer and Double");
	}

	public AbstractQueryResult performOperation(double number1, int number2) {
		throw new RuntimeException("Operation cannot be performed for Double and Integer");
	}

	public AbstractQueryResult performOperation(double number1, double number2) {
		throw new RuntimeException("Operation cannot be performed for two Doubles");
	}

	// Operations with Strings

	public AbstractQueryResult performOperation(int number, String string) {
		throw new RuntimeException("Operation cannot be performed for Integer and String");
	}

	public AbstractQueryResult performOperation(double number, String string) {
		throw new RuntimeException("Operation cannot be performed for Double and String");
	}

	public AbstractQueryResult performOperation(String string, int number) {
		throw new RuntimeException("Operation cannot be performed for String and Integer");
	}

	public AbstractQueryResult performOperation(String string, double number) {
		throw new RuntimeException("Operation cannot be performed for String and double");
	}

	public AbstractQueryResult performOperation(String string1, String string2) {
		throw new RuntimeException("Operation cannot be performed for two Strings");
	}

	// Operations with Booleans

	public AbstractQueryResult performOperation(int number, boolean bool) {
		throw new RuntimeException("Operation cannot be performed for Integer and Boolean");
	}

	public AbstractQueryResult performOperation(double number, boolean bool) {
		throw new RuntimeException("Operation cannot be performed for Double and Boolean");
	}

	public AbstractQueryResult performOperation(String string, boolean bool) {
		throw new RuntimeException("Operation cannot be performed for String and Boolean");
	}

	public AbstractQueryResult performOperation(boolean bool, int number) {
		throw new RuntimeException("Operation cannot be performed for Boolean and Integer");
	}

	public AbstractQueryResult performOperation(boolean bool, double number) {
		throw new RuntimeException("Operation cannot be performed for boolean and Double");
	}

	public AbstractQueryResult performOperation(boolean bool, String string) {
		throw new RuntimeException("Operation cannot be performed for String and Boolean");
	}

	public AbstractQueryResult performOperation(boolean bool1, boolean bool2) {
		throw new RuntimeException("Operation cannot be performed for two Booleans");
	}
}
