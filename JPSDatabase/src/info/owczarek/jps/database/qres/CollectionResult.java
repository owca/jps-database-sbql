package info.owczarek.jps.database.qres;

import java.util.Collection;

public abstract class CollectionResult extends AbstractQueryResult {
	public abstract Collection<AbstractQueryResult> getElements();

	public boolean contains(AbstractQueryResult someElement) {
		for (AbstractQueryResult collectionElement : getElements()) {
			if (collectionElement.equals(someElement)) {
				return true;
			}
		}
		return false;
	}
}
