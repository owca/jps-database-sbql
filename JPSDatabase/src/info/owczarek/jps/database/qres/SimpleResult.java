package info.owczarek.jps.database.qres;

public class SimpleResult<T> extends SingleResult {
	protected T value;

	public SimpleResult(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value.toString();
	}

	public static boolean simpleResultsAreComparable(SimpleResult<?> result1, SimpleResult<?> result2) {
		Object result1Object = result1.getValue();
		Object result2Object = result2.getValue();

		if (!(result1Object instanceof Integer) && !(result1Object instanceof Double)) {
			return false;
		}
		if (!(result2Object instanceof Integer) && !(result2Object instanceof Double)) {
			return false;
		}

		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SimpleResult<?>) {
			SimpleResult<?> anotherSimpleResult = (SimpleResult<?>) o;
			return value.equals(anotherSimpleResult.getValue());
		}
		return false;
	}
}
