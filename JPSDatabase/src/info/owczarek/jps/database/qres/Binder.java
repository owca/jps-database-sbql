package info.owczarek.jps.database.qres;

import org.apache.commons.lang3.StringUtils;

public class Binder extends SingleResult {
	private String name;
	private AbstractQueryResult value;

	public Binder(String name, AbstractQueryResult value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public AbstractQueryResult getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "<" + getName() + ", " + getValue().toString() + ">";
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Binder) {
			Binder binder2 = (Binder) o;
			boolean haveTheSameName = StringUtils.equals(getName(), binder2.getName());
			if (haveTheSameName) {
				boolean poinTheSameResult = getValue().equals(binder2.getValue());
			}
		}
		return false;
	}
}
