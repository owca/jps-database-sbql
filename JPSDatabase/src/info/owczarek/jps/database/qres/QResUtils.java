package info.owczarek.jps.database.qres;

import java.util.Collection;
import java.util.List;

import javax.management.RuntimeErrorException;

public class QResUtils {

	/**
	 * Sprawdza czy bag ma tylko jeden element
	 * 
	 * @param bag
	 * @return
	 */
	public static boolean isBagWithOneElement(BagResult bag) {
		return bag.getElements().size() == 1;
	}

	/**
	 * Rzutuje bag z pojedynczym elementem na SimpleResult
	 * 
	 * @param bag
	 *            bag z jednym elementem
	 * @return element wewnątrz baga
	 */
	public static SimpleResult<?> oneElementBagToSimpleResult(BagResult bag) {
		if (!isBagWithOneElement(bag)) {
			throw new RuntimeException("This bag does not contain one element");
		}

		AbstractQueryResult element = bag.getElements().iterator().next();

		if (element instanceof SimpleResult<?>) {
			return (SimpleResult<?>) element;
		} else {
			throw new RuntimeException("The only one element in the bag is not SimpleResult but "
					+ element.getClass().getSimpleName());
		}
	}

	/**
	 * Zamienia dany rezultat qres na baga. Jeśli jest to jakaś kolekcja, to zrobi z niej baga Jeśli jest to
	 * SingleResult, to zrobi element prosty
	 * 
	 * @param result
	 * @return
	 */
	public static BagResult convertToBag(AbstractQueryResult result) {
		BagResult bag = new BagResult();
		if (result instanceof CollectionResult) {
			CollectionResult collection = (CollectionResult) result;
			bag.addElements(collection.getElements());
		} else if (result instanceof SingleResult) {
			bag.addElement(result);
		} else {
			throw new RuntimeException("It's impossible to convert this object to struct");
		}

		return bag;
	}

	/**
	 * Porównuje dwa SimpeResult przechowujące liczby
	 * 
	 * @param result1
	 * @param result2
	 * @return 1 jeśli pierwsza liczba jest większa, 0 jeśli liczb są równe, -1 jeśli pierwsza liczba jest mniejsza
	 */
	public static int compareSimpleResults(SimpleResult<?> result1, SimpleResult<?> result2) {
		Object result1Object = result1.getValue();
		Object result2Object = result2.getValue();

		if (!(result1Object instanceof Number) || !(result2Object instanceof Number)) {
			throw new RuntimeException("Only numbers can be compared");
		}

		Double result1Double = ((Number) result1Object).doubleValue();
		Double result2Double = ((Number) result2Object).doubleValue();
		return result1Double.compareTo(result2Double);
	}

	/**
	 * Zamienia baga z jedną strukturą w środku na baga z elementami struktury w środku
	 * 
	 * @param bag
	 * @return
	 */
	public static BagResult unpackStructFromBag(BagResult bag) {
		Collection<AbstractQueryResult> bagElements = bag.getElements();

		boolean thereIsOnlyOneElementInABag = bagElements.size() == 1;

		if (thereIsOnlyOneElementInABag) {
			AbstractQueryResult elementFromBag = bagElements.iterator().next();

			boolean elementFromBagIsAStructure = elementFromBag instanceof StructResult;

			if (elementFromBagIsAStructure) {
				StructResult structure = (StructResult) elementFromBag;
				BagResult structBag = new BagResult();
				structBag.addElements(structure.getElements());
				return structBag;
			}
		}

		throw new RuntimeException("Bag does not contain one structure");
	}

	public static boolean collectionContainsReference(CollectionResult collection, ReferenceResult reference) {
		// TODO
		return true;
	}
}
