package info.owczarek.jps.database.qres;

public class IntegerResult extends SimpleResult<Integer> {
	public IntegerResult(int value) {
		super(value);
	}
}
