package info.owczarek.jps.database.qres;

import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAObject;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SimpleObject;

public class ReferenceResult extends SingleResult {
	private OID oidValue;
	private SBAStore sbaStore;

	public OID getOIDValue() {
		return oidValue;
	}

	public ReferenceResult(OID oid, SBAStore sbaStore) {
		this.oidValue = oid;
		this.sbaStore = sbaStore;
	}

	@Override
	public String toString() {
		SBAObject sbaObject = sbaStore.retrieve(oidValue);
		if (sbaObject != null && sbaObject instanceof SimpleObject<?>) {
			SimpleObject<?> simpleObject = (SimpleObject<?>) sbaObject;
			Object referencedValue = simpleObject.getValue();

			return "ref(" + getOIDValue() + ": " + referencedValue + ")";
		}

		return "ref(" + getOIDValue() + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ReferenceResult) {
			ReferenceResult anotherReference = (ReferenceResult) o;
			if (anotherReference.getOIDValue().equals(oidValue)) {
				return true;
			}
		}
		return false;
	}
}
