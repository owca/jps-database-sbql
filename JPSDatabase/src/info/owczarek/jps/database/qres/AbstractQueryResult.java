package info.owczarek.jps.database.qres;

abstract public class AbstractQueryResult {
	/**
	 * Konwertuje dany rezultat do struktury, jeśli jest taka możliwość.
	 * Ta metoda służy do nadpisania. Dla każdego typu, którego się nie da
	 * przekonwertować, rzuci wyjątkiem.
	 * @return
	 */
	public StructResult toStruct() {
		throw new RuntimeException("Object class " + getClass() + " can't be converted to structure");
	}
}
