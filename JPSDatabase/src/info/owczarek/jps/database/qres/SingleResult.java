package info.owczarek.jps.database.qres;

public class SingleResult extends AbstractQueryResult {
	@Override
	public StructResult toStruct() {
		StructResult struct = new StructResult();
		struct.addElement(this);
		return struct;
	}
}
