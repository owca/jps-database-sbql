package info.owczarek.jps.database.qres;

public class StringResult extends SimpleResult<String> {
	public StringResult(String value) {
		super(value);
	}
	
	@Override
	public String toString() {
		return "\"" + value.toString() + "\"";
	}
}
