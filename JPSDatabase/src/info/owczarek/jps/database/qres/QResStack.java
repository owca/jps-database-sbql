package info.owczarek.jps.database.qres;

import java.util.Stack;

public class QResStack {
	private Stack<AbstractQueryResult> stack;
	
	public QResStack() {
		stack = new Stack<AbstractQueryResult>();
	}
	
	public AbstractQueryResult pop() {
		return stack.pop();
	}
	
	public void push(AbstractQueryResult result) {
		stack.push(result);
	}
	
	public String toString() {
		if (stack.size() > 0) {
			return stack.toString();
		} else {
			return "qres is empty";
		}
	}
}
