package info.owczarek.jps.database.qres;

public class BooleanResult extends SimpleResult<Boolean> {
	public BooleanResult(boolean value) {
		super(value);
	}
}
