package info.owczarek.jps.database.qres;

public class DoubleResult extends SimpleResult<Double> {
	public DoubleResult(double value) {
		super(value);
	}
}
