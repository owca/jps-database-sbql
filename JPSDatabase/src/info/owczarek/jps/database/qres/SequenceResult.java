package info.owczarek.jps.database.qres;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SequenceResult extends CollectionResult {
	private List<AbstractQueryResult> elements;
	
	public SequenceResult() {
		elements = new ArrayList<AbstractQueryResult>();
	}
	
	public Collection<AbstractQueryResult> getElements() {
		return elements;
	}
}
