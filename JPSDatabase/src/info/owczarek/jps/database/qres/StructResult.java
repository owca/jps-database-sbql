package info.owczarek.jps.database.qres;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

public class StructResult extends SingleResult {
	private List<AbstractQueryResult> elements;

	public StructResult(AbstractQueryResult... abstractQueryResults) {
		elements = new ArrayList<AbstractQueryResult>(Arrays.asList(abstractQueryResults));
	}

	public StructResult(List<AbstractQueryResult> elements) {
		this.elements = elements;
	}

	public StructResult() {
		elements = new ArrayList<AbstractQueryResult>();
	}

	public void addElement(AbstractQueryResult result) {
		elements.add(result);
	}

	public void addElements(Collection<AbstractQueryResult> elementsToAdd) {
		elements.addAll(elementsToAdd);
	}

	public List<AbstractQueryResult> getElements() {
		return elements;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("struct(");

		String elementsAsString = StringUtils.join(elements, ", ");
		sb.append(elementsAsString);

		sb.append(")");
		return sb.toString();
	}

	@Override
	public StructResult toStruct() {
		return this;
	}

	public static StructResult createStructure(AbstractQueryResult e1, AbstractQueryResult e2) {
		StructResult structure = new StructResult();
		if ((e1 instanceof SimpleResult<?> || e1 instanceof ReferenceResult || e1 instanceof Binder)
				&& (e2 instanceof SimpleResult<?> || e2 instanceof ReferenceResult || e2 instanceof Binder)) {
			// 1, 2 => struct(1,2)
			structure.addElement(e1);
			structure.addElement(e2);
		} else if ((e1 instanceof SimpleResult<?> || e1 instanceof ReferenceResult || e1 instanceof Binder)
				&& e2 instanceof StructResult) {
			// 1, struct(2, 3) => struct(1,2,3)
			StructResult structE2 = (StructResult) e2;
			structure.addElement(e1);
			structure.addElements(structE2.getElements());
		} else if (e1 instanceof StructResult
				&& (e2 instanceof SimpleResult<?> || e2 instanceof ReferenceResult || e2 instanceof Binder)) {
			// struct(1,2), 3 => struct(1,2,3)
			StructResult structE1 = (StructResult) e1;
			structure.addElements(structE1.getElements());
			structure.addElement(e2);
		} else if (e1 instanceof StructResult && e2 instanceof StructResult) {
			// struct(1,2), struct(3,4) => struct(1,2,3,4)
			StructResult structE1 = (StructResult) e1;
			StructResult structE2 = (StructResult) e2;
			structure.addElements(structE1.getElements());
			structure.addElements(structE2.getElements());
		} else {
			throw new RuntimeException("Structures can be created from either SimpleResult or Structure results");
		}

		return structure;
	}

	public boolean contains(AbstractQueryResult someElement) {
		for (AbstractQueryResult collectionElement : getElements()) {
			if (collectionElement.equals(someElement)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof StructResult) {
			StructResult anotherStruct = (StructResult) o;

			List<AbstractQueryResult> anotherStructElements = anotherStruct.getElements();

			return getElements().equals(anotherStructElements);
		}

		return false;
	}
}
