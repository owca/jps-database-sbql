package info.owczarek.jps.database.qres;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BagResult extends CollectionResult {
	private List<AbstractQueryResult> elements;

	public BagResult() {
		elements = new ArrayList<AbstractQueryResult>();
	}

	public BagResult(List<AbstractQueryResult> elements) {
		this.elements = elements;
	}

	public void addElement(AbstractQueryResult element) {
		elements.add(element);
	}

	public void addElements(Collection<AbstractQueryResult> elements) {
		this.elements.addAll(elements);
	}

	public Collection<AbstractQueryResult> getElements() {
		return elements;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("bag(");
		if (elements.size() > 0) {
			sb.append(elements.get(0));
			for (int i = 1; i < elements.size(); i++) {
				sb.append(", ");
				sb.append(elements.get(i));
			}
		}
		sb.append(")");
		return sb.toString();
	}

	/**
	 * Zamienia baga na strukturę
	 */
	@Override
	public StructResult toStruct() {
		StructResult struct = new StructResult();
		struct.addElements(getElements());
		return struct;
	}

	/**
	 * Próbuje przekonwertować bag z pojedynczym elementem na SingleResult
	 * 
	 * @return
	 */
	public SingleResult convertToSingleResult() {
		if (elements.size() != 1) {
			throw new RuntimeException(
					"Bag cannot be converted to SingleResult because number of elements it contains is "
							+ elements.size());
		}

		AbstractQueryResult element = elements.get(0);

		if (element instanceof SingleResult) {
			return (SingleResult) element;
		} else {
			throw new RuntimeException("The element this bag contains is not SingleResult but " + element.getClass()
					+ " therefore it cannot be used to conversion");
		}
	}

	/**
	 * Sprawdza czy bag ma tylko jeden element, który jest typu SingleResult i można go przekonwertować do wspomnianego
	 * SingleResult
	 * 
	 * @return
	 */
	public boolean canBeConvertedToSingleResult() {
		if (elements.size() == 1 && elements.get(0) instanceof SingleResult) {
			return true;
		} else {
			return false;
		}
	}
}
