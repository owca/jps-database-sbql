package info.owczarek.jps.database.envs;

import info.owczarek.jps.database.qres.AbstractQueryResult;

public class ENVSBinder {
	private String name;
	private AbstractQueryResult value;
	
	public ENVSBinder(String name, AbstractQueryResult value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public AbstractQueryResult getValue() {
		return value;
	}
	
	public String toString() {
		return name + "(" + value + ")";
	}
}
