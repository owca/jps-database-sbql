package info.owczarek.jps.database.envs;

import java.util.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;

public class ENVS {
	private List<ENVSFrame> frameStack;

	public ENVS() {
		frameStack = new ArrayList();
	}

	/**
	 * Używane do zainicjalizowania ENVS wszystkimi obiektami "korzeniami"
	 * 
	 * @param oid
	 *            identyfikator obiektu od którego zaczyna się impreza
	 * @param sbaStore
	 *            stos sba
	 */
	public void init(OID oid, SBAStore sbaStore) {
		ComplexObject rootObject = (ComplexObject) sbaStore.retrieve(oid);
		List<OID> rootsOids = rootObject.getChildrenOids();

		List<ENVSBinder> rootBinders = new ArrayList();

		for (OID rootOid : rootsOids) {
			SBAObject rootSbaObject = sbaStore.retrieve(rootOid);
			
			ReferenceResult reference = new ReferenceResult(rootSbaObject.getOID(), sbaStore);
			String objectName = rootSbaObject.getName();

			ENVSBinder binder = new ENVSBinder(objectName, reference);
			rootBinders.add(binder);
		}

		ENVSFrame frame = new ENVSFrame(rootBinders);
		push(frame);
	}

	/**
	 * Zdejmuje sekcje ze szczytu stosu
	 */
	public ENVSFrame pop() {
		int lastFrameIndex = frameStack.size() - 1;
		ENVSFrame lastFrame = frameStack.get(lastFrameIndex);
		frameStack.remove(lastFrameIndex);
		return lastFrame;
	}

	/**
	 * Umieszcza sekcję na szczycie stosu
	 */
	public void push(ENVSFrame envsFrame) {
		frameStack.add(envsFrame);
	}

	public BagResult bind(String name) {
		for (int i = frameStack.size() - 1; i >= 0; i--) {
			ENVSFrame frame = frameStack.get(i);

			BagResult result = frame.getBindersByName(name);
			if (result != null) {
				return result;
			}
		}

		return new BagResult();
	}

	/**
	 * Produkuje na podstawie danego elementu listę binderów, które zawierają jego wnętrze. Metoda wykorzystywana do
	 * tworzenia nowej ramki na ENVS zawierającej wnętrze obiektu potraktowanego operatorem kropki. Dzięki temu, w
	 * aktualnym środowisku można się odwoływać do elementów wewnętrznych tego obiektu
	 * 
	 * @param result
	 *            obiekt do którego wnętrza zaglądamy
	 * @param sbaStore
	 *            stos obiektów bazy danych
	 * @return listę binderów z wnętrzem obiektu danego typu
	 */
	public List<ENVSBinder> nested(AbstractQueryResult result, SBAStore sbaStore) {
		List<ENVSBinder> binders;

		if (result instanceof CollectionResult) {
			CollectionResult collectionResult = (CollectionResult) result;
			binders = nestedForCollectionResult(collectionResult, sbaStore);
		} else if (result instanceof ReferenceResult) {
			ReferenceResult referenceResult = (ReferenceResult) result;
			binders = nestedForReferenceResult(referenceResult, sbaStore);
		} else if (result instanceof Binder) {
			Binder binder = (Binder) result;
			binders = nestedForBinder(binder);
		} else if (result instanceof StructResult) {
			StructResult structResult = (StructResult) result;
			binders = nestedForStructResult(structResult, sbaStore);
		} else {
			// Obiekt prosty, wartość atomowa, coś innego. Zaglądanie do takich
			// obiektów operatorem
			// kropki nie ma sensu, bo nie ma w nich wnętrzu żadnych obiektów
			binders = new ArrayList<ENVSBinder>();
		}

		return binders;
	}

	/**
	 * Zwraca bindery zawierajace sumę elementów wewnętrznych kolekcji
	 * 
	 * @param collectionResult
	 *            kolekcja zawierająca jakieś elementy
	 * @param sbaStore
	 * @return
	 */
	private List<ENVSBinder> nestedForCollectionResult(CollectionResult collectionResult, SBAStore sbaStore) {
		List<ENVSBinder> result = new ArrayList<ENVSBinder>();

		Collection<AbstractQueryResult> elements = collectionResult.getElements();
		for (AbstractQueryResult element : elements) {
			List<ENVSBinder> elementResult = nested(element, sbaStore);
			result.addAll(elementResult);
		}

		return result;
	}

	/**
	 * Daje nested dla obiektu-referencji. Zwraca binder dla wskazywanego obiektu
	 */
	private List<ENVSBinder> nestedForReferenceResult(ReferenceResult referenceResult, SBAStore sbaStore) {
		List<ENVSBinder> binders = new ArrayList<ENVSBinder>();
		
		OID referencedObjectOid = referenceResult.getOIDValue();
		SBAObject referencedObject = sbaStore.retrieve(referencedObjectOid);

		if (referencedObject instanceof SimpleObject<?>) {
			String referenceObjectName = referencedObject.getName();
			ENVSBinder binder = new ENVSBinder(referenceObjectName, new ReferenceResult(referencedObjectOid, sbaStore));
			binders.add(binder);
		} else if (referencedObject instanceof ComplexObject) {
			ComplexObject referencedComplexObject = (ComplexObject) referencedObject;
			List<OID> childrenOids = referencedComplexObject.getChildrenOids();
			
			for(OID childOid: childrenOids) {
				SBAObject childObject = sbaStore.retrieve(childOid);
				String childName = childObject.getName();
				ENVSBinder binder = new ENVSBinder(childName, new ReferenceResult(childOid, sbaStore));
				binders.add(binder);
			}
		}
		
		

		return binders;
	}

	/**
	 * Wykonuje operację nested dla obiektów - binderów
	 */
	private List<ENVSBinder> nestedForBinder(Binder binder) {
		String binderName = binder.getName();
		AbstractQueryResult binderValue = binder.getValue();
		ENVSBinder envsBinder = new ENVSBinder(binderName, binderValue);

		List<ENVSBinder> binders = new ArrayList(1);
		binders.add(envsBinder);

		return binders;
	}

	/**
	 * Wykonuje operację nested dla struktury. Daje sumę operacji nested każdego z jej elementów
	 */
	private List<ENVSBinder> nestedForStructResult(StructResult structResult, SBAStore sbaStore) {
		List<ENVSBinder> result = new ArrayList<ENVSBinder>();

		Collection<AbstractQueryResult> elements = structResult.getElements();
		for (AbstractQueryResult element : elements) {
			List<ENVSBinder> elementResult = nested(element, sbaStore);
			result.addAll(elementResult);
		}

		return result;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (ENVSFrame frame : frameStack) {
			builder.append(frame.toString());
			builder.append("\n");
		}

		return builder.toString();

	}
}
