package info.owczarek.jps.database.envs;

import info.owczarek.jps.database.qres.*;

import java.util.*;

import org.apache.commons.lang3.StringUtils;

public class ENVSFrame {
	private List<ENVSBinder> binders;
	
	public ENVSFrame() {
		binders = new ArrayList<ENVSBinder>();
	}

	public ENVSFrame(List<ENVSBinder> binders) {
		this.binders = binders;
	}
	
	/**
	 * Dodaje jeden binder do ramki
	 * @param binder binder do dodania
	 */
	public void add(ENVSBinder binder) {
		binders.add(binder);
	}
	
	/**
	 * Dodaje kolekcję binderów do ramki
	 * @param bindery do dodania
	 */
	public void add(Collection<ENVSBinder> binders) {
		for (ENVSBinder binder: binders) {
			add(binder);
		}
	}

	public Collection<ENVSBinder> getElements() {
		return binders;
	}

	/**
	 * Zwraca wszystkie bindery w ramce, które posiadają określoną nazwę
	 * 
	 * @return BagResult zawierający elementy binderów lub null w przypadku
	 *         braku
	 */
	public BagResult getBindersByName(String binderName) {
		List<AbstractQueryResult> bagElements = new ArrayList<AbstractQueryResult>();

		for (ENVSBinder binder : getElements()) {
			String currentBinderName = binder.getName();
			if (StringUtils.equals(binderName, currentBinderName)) {
				bagElements.add(binder.getValue());
			}
		}

		if (bagElements.size() > 0) {
			BagResult result = new BagResult(bagElements);
			return result;
		} else {
			return null;
		}
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("=================================\n");
		
		for(ENVSBinder binder: binders) {
			builder.append(binder.toString());
			builder.append("\n");
		}
		
		builder.append("=================================");
		
		return builder.toString();
	}
}
