package info.owczarek.jps.database.store;

public class SimpleObject<T> extends SBAObject {
	private T value;
	
	public SimpleObject(String name, T value) {
		super(name);
		this.value = value;
	}
	
	public T getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		String valueRepresentation = value instanceof String ? "\"" + getValue() + "\"" : getValue().toString();
		return "<" + getOID() + ", " + getName() + ", " + valueRepresentation + ">";
	}
}
