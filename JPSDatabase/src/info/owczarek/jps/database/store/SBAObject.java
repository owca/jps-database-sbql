package info.owczarek.jps.database.store;

public abstract class SBAObject {
	protected String name;
	private OID oid;
	
	protected SBAObject(String name) {
		this.name = name;
		this.oid = new OID();
	}

	public String getName() {
		return this.name;
	}

	public OID getOID() {
		return this.oid;
	}

}
