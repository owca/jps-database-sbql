package info.owczarek.jps.database.store;

import java.util.Collection;
import java.util.List;

public interface SBAStore {
	public SBAObject retrieve(OID oid);
	public OID loadXML(String filePath);
	public OID addJavaObject(Object object, String name);
	public List<OID> addJavaCollection(Collection collection, String name);
	public void clear();
}
