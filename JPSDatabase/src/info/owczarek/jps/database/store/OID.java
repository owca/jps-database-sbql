package info.owczarek.jps.database.store;

public class OID {
	private static long nextNum = 0;
	private static final String prefix = "i";
	private long num;

	public OID() {
		num = nextNum++;
	}

	public String toString() {
		return prefix + num;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (num ^ (num >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OID other = (OID) obj;
		if (num != other.num)
			return false;
		return true;
	}

	public static void restart() {
		nextNum = 0;
	}
}
