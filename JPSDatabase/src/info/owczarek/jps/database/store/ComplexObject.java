package info.owczarek.jps.database.store;

import org.apache.commons.lang3.StringUtils;
import java.util.*;

public class ComplexObject extends SBAObject {
	private List<OID> childrenOids;

	protected ComplexObject(String name, List<OID> childrenOids) {
		super(name);
		this.childrenOids = childrenOids;
	}

	public List<OID> getChildrenOids() {
		return childrenOids;
	}
	
	@Override
	public String toString() {
		String childrenOidsAsString = StringUtils.join(getChildrenOids(), ", ");
		return "<" + getOID() + ", " + getName() + ", {" + childrenOidsAsString + "}>";
	}
}
