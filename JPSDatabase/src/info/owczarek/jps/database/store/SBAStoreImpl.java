package info.owczarek.jps.database.store;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SBAStoreImpl implements SBAStore {
	private Map<OID, SBAObject> objects;

	public SBAStoreImpl() {
		this.objects = new HashMap<OID, SBAObject>();
	}

	@Override
	public SBAObject retrieve(OID oid) {
		return objects.get(oid);
	}

	@Override
	public OID loadXML(String filePath) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = null;
		try {
			db = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		}

		Document dom = null;
		try {
			dom = db.parse(filePath);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		OID rootElementOid = parseElement(dom.getDocumentElement());
		return rootElementOid;
	}
	
	private OID parseElement(Node node) {
		OID nodeOid = null;
		SBAObject nodeSBAObject = null;
		
		String elementName = node.getNodeName();
		NodeList children = node.getChildNodes();
		
		// Jeśli coś ma więcej niż jeden węzeł, to znaczy, że ma w sobie jakieś dodatkowe elementy
		// Jeśli ma tylko jeden węzeł, to znaczy, że zawartością jest sam tekst
		if (children.getLength() > 1) {
			List<OID> childrenOids = new ArrayList();
			for (int x = 0; x < children.getLength(); x++) {
				Node child = children.item(x);
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					OID childOid = parseElement(child);
					childrenOids.add(childOid);
				}
			}
			nodeSBAObject = new ComplexObject(elementName, childrenOids);
		} else {
			
			String textValue = node.getTextContent();
			textValue = textValue.trim();
			
			Class contentClass = whatTypeStringContains(textValue);
			
			if (Integer.class == contentClass) {
				Integer nodeObject = Integer.parseInt(textValue);
				nodeSBAObject = new SimpleObject<Integer>(elementName, nodeObject);
			} else if (Double.class == contentClass) {
				Double nodeObject = Double.parseDouble(textValue);
				nodeSBAObject = new SimpleObject<Double>(elementName, nodeObject);
			} else if (Boolean.class == contentClass) {
				Boolean nodeObject = Boolean.parseBoolean(textValue);
				nodeSBAObject = new SimpleObject<Boolean>(elementName, nodeObject);
			} else {
				String nodeObject = textValue;
				nodeSBAObject = new SimpleObject<String>(elementName, nodeObject);
			}
			
		}
		
		nodeOid = nodeSBAObject.getOID();
		objects.put(nodeOid, nodeSBAObject);
		return nodeOid;
	}
	
	private Class whatTypeStringContains(String string) {
		try {
			Integer.parseInt(string);
			return Integer.class;
		} catch (NumberFormatException ex) {}
		
		try {
			Double.parseDouble(string);
			return Double.class;
		} catch (NumberFormatException ex) {}
		
		String lowerCase = string.toLowerCase();
		if (lowerCase.equals("true") || lowerCase.equals("false")) {
			return Boolean.class;
		}
		
		return String.class;
	}

	@Override
	public OID addJavaObject(Object object, String name) {
		Class objectClass = object.getClass();

		SBAObject sbaObject = null;
		OID sbaObjectOid = null;

		if (Integer.class == objectClass) {
			sbaObject = new SimpleObject<Integer>(name, (Integer) object);
		} else if (Double.class == objectClass) {
			sbaObject = new SimpleObject<Double>(name, (Double) object);
		} else if (String.class == objectClass) {
			sbaObject = new SimpleObject<String>(name, (String) object);
		} else if (Boolean.class == objectClass) {
			sbaObject = new SimpleObject<Boolean>(name, (Boolean) object);
		} else {
			Field[] objectFields = objectClass.getFields();
			List<OID> childrenOids = new ArrayList(objectFields.length);

			for (Field objectField : objectFields) {
				try {
					String childName = objectField.getName();
					Object childObject = objectField.get(object);

					OID childObjectOid = addJavaObject(childObject, childName);
					childrenOids.add(childObjectOid);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}

			sbaObject = new ComplexObject(name, childrenOids);
		}

		sbaObjectOid = sbaObject.getOID();
		objects.put(sbaObjectOid, sbaObject);

		return sbaObjectOid;
	}

	@Override
	public List<OID> addJavaCollection(Collection collection, String name) {
		List<OID> childrenOids = new ArrayList(collection.size());

		for (Object object : collection) {
			OID childOid = addJavaObject(object, name);
			childrenOids.add(childOid);
		}

		return childrenOids;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("==================SBAStore===================\n");

		for (SBAObject object : objects.values()) {
			builder.append("| ");
			builder.append(object.toString());
			builder.append("\n");
		}

		builder.append("==============================================");

		return builder.toString();
	}

	@Override
	public void clear() {
		objects = new HashMap<OID, SBAObject>();
	}

}
