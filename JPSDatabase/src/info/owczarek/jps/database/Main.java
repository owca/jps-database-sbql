package info.owczarek.jps.database;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.envs.ENVS;
import info.owczarek.jps.database.parser.SBQLCup;
import info.owczarek.jps.database.qres.AbstractQueryResult;
import info.owczarek.jps.database.qres.QResStack;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SBAStoreImpl;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ENVS envs = new ENVS();
		SBAStore sbaStore = new SBAStoreImpl();
		QResStack qres = new QResStack();
		OID rootOid = sbaStore.loadXML("dane_do_zap_testowych.xml");
		envs.init(rootOid, sbaStore);
		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);
		
		System.out.println(sbaStore);
		
		do {
			try {
				System.out.println("> ");
				String query = scanner.nextLine();
				Expression expression = createASTFromExpression(query);
				expression.accept(visitor);
				AbstractQueryResult result = qres.pop();
				System.out.println(result.toString());
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		} while (scanner.hasNextLine());
	}
	
	public static Expression createASTFromExpression(String expression) {
		SBQLCup parser = new SBQLCup(expression);
		try {
			parser.user_init();
			Expression result = (Expression) parser.parse().value;
			return result;
		} catch (Exception e) {
			throw new RuntimeException("Error while creating AST from " + expression + ":" + e.getMessage());
		}
	}

}
