package info.owczarek.jps.database.test;

import info.owczarek.jps.database.parser.SBQLCup;
import info.owczarek.jps.database.parser.SBQLLexer;

public class ParserTest {

	public static void main(String[] args) {
		
		
		SBQLCup parser = new SBQLCup("3.6");
		try {
			parser.user_init();
			Object result = parser.parse().value;
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
