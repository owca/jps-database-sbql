/**
 * Pakiet zawiera testy wykorzystywane w ramach zaliczanych zadań 1-4 oraz testy dodatkowe sprawdzajace przebieg wykonania napisanego kodu. Testy dla zadań 5 i 6 znajdują się w folderze test (a nie src)
 */
package info.owczarek.jps.database.test;