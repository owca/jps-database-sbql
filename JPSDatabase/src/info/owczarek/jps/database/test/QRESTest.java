package info.owczarek.jps.database.test;

import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.qres.*;

public class QRESTest {

	public static void main(String[] args) {
		test1();
		test2();
		test3();
	}

	public static void test1() {
		// (struct(1, 2 + 10, (bag("test", "ala") as nazwa));
		QResStack stack = new QResStack();

		stack.push(new IntegerResult(1));

		stack.push(new IntegerResult(2));
		stack.push(new IntegerResult(1));
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			stack.push(new IntegerResult(resLeft.getValue()
					+ resRight.getValue()));
		}
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}
		{
			stack.push(new StringResult("test"));
			stack.push(new StringResult("Ala"));
			StringResult resRight = (StringResult) stack.pop();
			StringResult resLeft = (StringResult) stack.pop();
			StructResult result = ComaExpression.cartesianProduct(resLeft, resRight);
			stack.push(result);
		}
		{
			StructResult res = (StructResult) stack.pop();
			stack.push(new BagResult(res.getElements()));
		}
		{
			BagResult res = (BagResult) stack.pop();
			stack.push(new Binder("nazwa", res));
		}
		{
			Binder resRight = (Binder) stack.pop();
			StructResult resLeft = (StructResult) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}
		StructResult res = (StructResult) stack.pop();
		System.out.println(res.toString());
	}

	public static void test2() {
		// (bag("ala" + "ma" + "kota")), bag(8*10, false)
		QResStack stack = new QResStack();

		{
			stack.push(new StringResult("ala"));
			stack.push(new StringResult("ma"));
		}
		{
			StringResult resRight = (StringResult) stack.pop();
			StringResult resLeft = (StringResult) stack.pop();
			stack.push(new StringResult(resLeft.getValue()
					+ resRight.getValue()));
			stack.push(new StringResult("kota"));
		}
		{
			StringResult resRight = (StringResult) stack.pop();
			StringResult resLeft = (StringResult) stack.pop();
			stack.push(new StringResult(resLeft.getValue()
					+ resRight.getValue()));
		}
		{
			StringResult res = (StringResult) stack.pop();
			BagResult result = new BagResult();
			result.addElement(res);
			stack.push(result);
		}
		{
			stack.push(new IntegerResult(8));
			stack.push(new IntegerResult(10));
		}
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			stack.push(new IntegerResult(resLeft.getValue()
					* resRight.getValue()));
		}
		{
			stack.push(new BooleanResult(false));
		}
		{
			BooleanResult resRight = (BooleanResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}
		{
			StructResult res = (StructResult) stack.pop();
			stack.push(new BagResult(res.getElements()));
		}
		{
			BagResult resRight = (BagResult) stack.pop();
			BagResult resLeft = (BagResult) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}

		BagResult res = (BagResult) stack.pop();
		System.out.println(res);
	}

	public static void test3() {
		// (bag(1, 2) groupas(x)), bag(3, 4), 5
		QResStack stack = new QResStack();
		{
			stack.push(new IntegerResult(1));
			stack.push(new IntegerResult(2));
		}
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			BagResult res = new BagResult();
			res.addElement(resLeft);
			res.addElement(resRight);
			stack.push(res);
		}
		{
			BagResult res = (BagResult) stack.pop();
			stack.push(new Binder("x", res));
		}
		{
			stack.push(new IntegerResult(3));
			stack.push(new IntegerResult(4));
		}
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			IntegerResult resLeft = (IntegerResult) stack.pop();
			BagResult res = new BagResult();
			res.addElement(resLeft);
			res.addElement(resRight);
			stack.push(res);
		}
		{
			BagResult resRight = (BagResult) stack.pop();
			Binder resLeft = (Binder) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}
		{
			stack.push(new IntegerResult(5));
		}
		{
			IntegerResult resRight = (IntegerResult) stack.pop();
			BagResult resLeft = (BagResult) stack.pop();
			stack.push(ComaExpression.cartesianProduct(resLeft, resRight));
		}

		System.out.println(stack.pop());
	}
}
