package info.owczarek.jps.database.test;

import info.owczarek.jps.database.store.*;
import info.owczarek.jps.database.test.sbastoretestclasses.Samochod;
import info.owczarek.jps.database.test.sbastoretestclasses.SomeClass;

public class SBAStoreTest {

	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();

		System.out.println("Adding some simple objects");
		OID someInteger = sbaStore.addJavaObject(5, "someInteger");
		System.out.println(sbaStore.retrieve(someInteger));

		OID someDouble = sbaStore.addJavaObject(1.0, "someDouble");
		System.out.println(sbaStore.retrieve(someDouble));

		OID someBoolean = sbaStore.addJavaObject(true, "someBoolean");
		System.out.println(sbaStore.retrieve(someBoolean));

		OID someString = sbaStore.addJavaObject("someString", "someString");
		System.out.println(sbaStore.retrieve(someString));

		
		System.out.println("\n === Store clear ===\n");
		System.out.println("Adding complex object");
		sbaStore.clear();
		OID someComplexObject = sbaStore.addJavaObject(new SomeClass(),
				"someComplexObject");
		System.out.println(sbaStore.retrieve(someComplexObject));
		
		System.out.println(sbaStore.toString());
		
		System.out.println("\n === Store clear ===\n");
		System.out.println("Adding more sophisticated object");
		sbaStore.clear();
		
		Samochod samochod = new Samochod();
		sbaStore.addJavaObject(samochod, "autko");
		System.out.println(sbaStore.toString());
		
		System.out.println("\n === Store clear ===\n");
		System.out.println("Reading xml file");
		sbaStore.clear();
		
		sbaStore.loadXML("biblioteka.xml");
		System.out.println(sbaStore.toString());
	}

}