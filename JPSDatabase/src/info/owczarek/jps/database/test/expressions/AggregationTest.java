package info.owczarek.jps.database.test.expressions;


import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.terminal.DoubleTerminal;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.unary.AvgExpression;
import info.owczarek.jps.database.ast.unary.MaxExpression;
import info.owczarek.jps.database.ast.unary.MinExpression;
import info.owczarek.jps.database.ast.unary.SumExpression;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.envs.ENVS;
import info.owczarek.jps.database.qres.AbstractQueryResult;
import info.owczarek.jps.database.qres.BagResult;
import info.owczarek.jps.database.qres.IntegerResult;
import info.owczarek.jps.database.qres.QResStack;
import info.owczarek.jps.database.qres.QResUtils;
import info.owczarek.jps.database.qres.SimpleResult;
import info.owczarek.jps.database.qres.StructResult;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SBAStoreImpl;

public class AggregationTest {

	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		QResStack qres = new QResStack();
		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);

		// sum(1, 2, 3, 4, 5)
		Expression intElements = new ComaExpression(new IntegerTerminal(1), new ComaExpression(new IntegerTerminal(2),
				new ComaExpression(new IntegerTerminal(3), new ComaExpression(new IntegerTerminal(4),
						new IntegerTerminal(5)))));

		Expression intSum = new SumExpression(intElements);
		intSum.accept(visitor);
		System.out.println("intSum");
		System.out.println(qres.pop());
		
		Expression intMin = new MinExpression(intElements);
		intMin.accept(visitor);
		System.out.println("intMin");
		System.out.println(qres.pop());
		
		Expression intMax = new MaxExpression(intElements);
		intMax.accept(visitor);
		System.out.println("intMax");
		System.out.println(qres.pop());
		
		Expression intAvg = new AvgExpression(intElements);
		intAvg.accept(visitor);
		System.out.println("intAvg");
		System.out.println(qres.pop());
		
		Expression doubleElements = new ComaExpression(new DoubleTerminal(1.0), new ComaExpression(new DoubleTerminal(2.0),
				new ComaExpression(new DoubleTerminal(3.0), new ComaExpression(new DoubleTerminal(4.0),
						new DoubleTerminal(5.0)))));
		
		Expression doubleSum = new SumExpression(doubleElements);
		doubleSum.accept(visitor);
		System.out.println("doubleSum");
		System.out.println(qres.pop());
		
		Expression doubleMin = new MinExpression(doubleElements);
		doubleMin.accept(visitor);
		System.out.println("doubleMin");
		AbstractQueryResult x = qres.pop();
		System.out.println(x);
		
		Expression doubleMax = new MaxExpression(doubleElements);
		doubleMax.accept(visitor);
		System.out.println("doubleMax");
		System.out.println(qres.pop());
		
		Expression doubleAvg = new AvgExpression(doubleElements);
		doubleAvg.accept(visitor);
		System.out.println("doubleAvg");
		System.out.println(qres.pop());
		
		
	}
}
