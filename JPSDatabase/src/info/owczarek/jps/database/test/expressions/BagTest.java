package info.owczarek.jps.database.test.expressions;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.terminal.DoubleTerminal;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.envs.ENVS;
import info.owczarek.jps.database.qres.QResStack;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SBAStoreImpl;

public class BagTest {

	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		QResStack qres = new QResStack();
		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		System.out.println(sbaStore);

		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);

		
		Expression bagExpression = new BagExpression(new IntegerTerminal(12));
		bagExpression.accept(visitor);
		System.out.println(qres);
		
		Expression bagExpression2 = new BagExpression(
				new ComaExpression(
						new IntegerTerminal(1),
						new ComaExpression(
								new IntegerTerminal(2),
								new IntegerTerminal(3)
								)
						)
				);
		bagExpression2.accept(visitor);
		System.out.println(qres);
		
		Expression comaExpression = new ComaExpression(
				new IntegerTerminal(1),
				new ComaExpression(
						new IntegerTerminal(2),
						new IntegerTerminal(3)
						)
				);
		comaExpression.accept(visitor);
		System.out.println(qres);
		
		
	}
}
