package info.owczarek.jps.database.test.expressions;


import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.envs.ENVS;
import info.owczarek.jps.database.qres.BagResult;
import info.owczarek.jps.database.qres.IntegerResult;
import info.owczarek.jps.database.qres.QResStack;
import info.owczarek.jps.database.qres.QResUtils;
import info.owczarek.jps.database.qres.SimpleResult;
import info.owczarek.jps.database.qres.StructResult;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SBAStoreImpl;

public class VariousTests {

	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		QResStack qres = new QResStack();
		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);

		Expression coma = new ComaExpression(new IntegerTerminal(1), new ComaExpression(new IntegerTerminal(2),
				new ComaExpression(new IntegerTerminal(3), new ComaExpression(new IntegerTerminal(4),
						new IntegerTerminal(5)))));

		coma.accept(visitor);
		System.out.println(qres);
	}
}
