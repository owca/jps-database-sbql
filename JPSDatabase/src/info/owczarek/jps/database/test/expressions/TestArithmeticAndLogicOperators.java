package info.owczarek.jps.database.test.expressions;

import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.arithmetic.AddExpression;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.binary.comparison.GreaterExpression;
import info.owczarek.jps.database.ast.binary.comparison.NotEqualExpression;
import info.owczarek.jps.database.ast.terminal.DoubleTerminal;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.envs.ENVS;
import info.owczarek.jps.database.qres.BagResult;
import info.owczarek.jps.database.qres.DoubleResult;
import info.owczarek.jps.database.qres.QResStack;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAStore;
import info.owczarek.jps.database.store.SBAStoreImpl;

public class TestArithmeticAndLogicOperators {

	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		QResStack qres = new QResStack();
		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		System.out.println(sbaStore);

		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);

		
		Expression equalExpression1 = new EqualExpression(new IntegerTerminal(10), new IntegerTerminal(20));
		equalExpression1.accept(visitor);
		System.out.println("10 = 20");
		System.out.println(qres);
		
		Expression equalExpression2 = new EqualExpression(new IntegerTerminal(10), new IntegerTerminal(10));
		equalExpression2.accept(visitor);
		System.out.println("10 = 10");
		System.out.println(qres);
		
		
		Expression notEqualExpression1 = new NotEqualExpression(new IntegerTerminal(10), new IntegerTerminal(20));
		notEqualExpression1.accept(visitor);
		System.out.println("10 <> 20");
		System.out.println(qres);
		
		Expression notEqualExpression2 = new NotEqualExpression(new IntegerTerminal(10), new IntegerTerminal(10));
		notEqualExpression2.accept(visitor);
		System.out.println("10 <> 10");
		System.out.println(qres);
		
		Expression greaterExpression1 = new GreaterExpression(new IntegerTerminal(10), new DoubleTerminal(11.2));
		greaterExpression1.accept(visitor);
		System.out.println("10 > 11.2");
		System.out.println(qres);
		
		Expression greaterExpression2 = new GreaterExpression(new DoubleTerminal(10.1), new IntegerTerminal(15));
		greaterExpression2.accept(visitor);
		System.out.println("10.0 > 15");
		System.out.println(qres);
		
		Expression greaterExpression3 = new GreaterExpression(new IntegerTerminal(15), new DoubleTerminal(11.2));
		greaterExpression3.accept(visitor);
		System.out.println("15 > 11.2");
		System.out.println(qres);
		
		Expression greaterExpression4 = new GreaterExpression(new DoubleTerminal(23.0), new IntegerTerminal(15));
		greaterExpression4.accept(visitor);
		System.out.println("23.0 > 15");
		System.out.println(qres);
	}

}
