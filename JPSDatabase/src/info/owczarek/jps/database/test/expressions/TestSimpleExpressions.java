package info.owczarek.jps.database.test.expressions;

import info.owczarek.jps.database.envs.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;
import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.terminal.BooleanTerminal;
import info.owczarek.jps.database.ast.terminal.DoubleTerminal;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.terminal.StringTerminal;
import info.owczarek.jps.database.ast.visitor.ASTVisitor;

public class TestSimpleExpressions {
	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		QResStack qres = new QResStack();
		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);
		
		System.out.println(sbaStore);
		
		ASTVisitor visitor = new ASTVisitor(sbaStore, qres, envs);
		
		Expression stringEx = new StringTerminal("some string");
		stringEx.accept(visitor);
		System.out.println(qres);
		
		Expression integerEx = new IntegerTerminal(100);
		integerEx.accept(visitor);
		System.out.println(qres);
		
		Expression doubleEx = new DoubleTerminal(123.0);
		doubleEx.accept(visitor);
		System.out.println(qres);
		
		Expression booleanEx = new BooleanTerminal(true);
		booleanEx.accept(visitor);
		System.out.println(qres);
	}
}
