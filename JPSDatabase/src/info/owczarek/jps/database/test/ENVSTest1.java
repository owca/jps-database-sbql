package info.owczarek.jps.database.test;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import info.owczarek.jps.database.envs.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;

public class ENVSTest1 {

	// ((emp where married) union (emp where lName="Nowak")).fName
	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		System.out.println(sbaStore);

		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		QResStack qres = new QResStack();

		// (emp where married)
		{
			// 1. Zainicjować whereResult = bag()
			BagResult whereResult = new BagResult();
			
			// 2. Wykonać eval(q1) i zrobić q1res = QRES.POP()
			BagResult evalQ1 = envs.bind("emp");
			qres.push(evalQ1);
			BagResult q1Result = (BagResult) qres.pop();
			
			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for(AbstractQueryResult x: q1Result.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);
				
				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(x, sbaStore);
				frame.add(frameBinders);
				
				// 3.3.Wykonać eval(q2)
				BagResult allMarried = envs.bind("married");
				ReferenceResult referenceToMarried = (ReferenceResult)allMarried.getElements().iterator().next();
				SimpleObject<Boolean> marriedObject = (SimpleObject<Boolean>) sbaStore.retrieve(referenceToMarried.getOIDValue());
				Boolean marriedObjectValue = marriedObject.getValue();
				BooleanResult evalQ2 = new BooleanResult(marriedObjectValue);
				qres.push(evalQ2);
				
				// 3.3 [...] i zrobić q2res = QRES.POP()
				BooleanResult q2Res = (BooleanResult) qres.pop();
				
				// 3.4. Jeżeli q2res wynosi true dodać x do whereres
				if (q2Res.getValue() == true) {
					whereResult.addElement(x);
				}
				
				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}
			
			// 4. Zrobić QRES.PUSH(whereres)
			qres.push(whereResult);
		}
		
		// (emp where lName="Nowak")
		{
			// 1. Zainicjować whereResult = bag()
			BagResult whereResult = new BagResult();
			
			// 2. Wykonać eval(q1) i zrobić q1res = QRES.POP()
			BagResult evalQ1 = envs.bind("emp");
			qres.push(evalQ1);
			BagResult q1Result = (BagResult) qres.pop();
			
			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for (AbstractQueryResult x: q1Result.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);
				
				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(x, sbaStore);
				frame.add(frameBinders);
				
				// 3.3.Wykonać eval(q2)
				BagResult lNameBag = envs.bind("lName");
				ReferenceResult referenceToLName = (ReferenceResult) lNameBag.getElements().iterator().next();
				SimpleObject<String> lNameObject = (SimpleObject<String>) sbaStore.retrieve(referenceToLName.getOIDValue());
				String lNameObjectValue = lNameObject.getValue();
				Boolean evalQ2Value = StringUtils.equals("Nowak", lNameObjectValue);
				BooleanResult evalQ2 = new BooleanResult(evalQ2Value);
				qres.push(evalQ2);
				
				// 3.3 [...] i zrobić q2res = QRES.POP()
				BooleanResult q2res = (BooleanResult) qres.pop();
				
				// 3.4. Jeżeli q2res wynosi true dodać x do whereres
				if (q2res.getValue() == true) {
					whereResult.addElement(x);
				}
				
				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}
			
			// 4. Zrobić QRES.PUSH(whereres)
			qres.push(whereResult);
		}
		
		
		// union
		// (emp where married) UNION (emp where lName="Nowak")
		// qres(0) UNION qres(1)
		{
			// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
			BagResult eres = new BagResult();
			
			// 2. Wykonaj oba podwyrażenia. - już zrobione
			// 3. Podnieś dwa elementy z QRES.
			BagResult qres1 = (BagResult)qres.pop();
			BagResult qres2 = (BagResult)qres.pop();
			
			for (AbstractQueryResult element: qres1.getElements()) {
				eres.addElement(element);
			}
			
			for (AbstractQueryResult element: qres2.getElements()) {
				eres.addElement(element);
			}
			
			qres.push(eres);
		}
		
		// .
		// ((emp where married) union (emp where lName="Nowak")).fName
		{
			// 1. Zainicjalizować dotres = bag()
			BagResult dotResult = new BagResult();
			
			// 2. Wykonać eval(q1) i zrobić q1res = QRES.POP()
			BagResult q1res = (BagResult)qres.pop();
			
			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for(AbstractQueryResult x: q1res.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);
				
				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(x, sbaStore);
				frame.add(frameBinders);
				
				// 3.3.Wykonać eval(q2) [...]
				ReferenceResult evalQ2 = (ReferenceResult) envs.bind("fName").getElements().iterator().next();
				
				qres.push(evalQ2);
				
				// 3.3 [...]  i zrobić q2res = QRES.POP()
				ReferenceResult q2Res = (ReferenceResult) qres.pop();
				
				// 3.4.Dodać q2res do dotres (dotres ∪ q2res)
				dotResult.addElement(q2Res);
				
				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}
			// 4. Zrobić QRES.PUSH(dotres)
			qres.push(dotResult);
		}
		
		System.out.println(qres);
	}
}
