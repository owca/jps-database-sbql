package info.owczarek.jps.database.test;

import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.binary.DotExpression;
import info.owczarek.jps.database.ast.binary.InExpression;
import info.owczarek.jps.database.ast.binary.WhereExpression;
import info.owczarek.jps.database.ast.binary.arithmetic.AddExpression;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.binary.comparison.GreaterExpression;
import info.owczarek.jps.database.ast.terminal.DoubleTerminal;
import info.owczarek.jps.database.ast.terminal.IntegerTerminal;
import info.owczarek.jps.database.ast.terminal.NameTerminal;
import info.owczarek.jps.database.ast.terminal.StringTerminal;
import info.owczarek.jps.database.ast.unary.AvgExpression;
import info.owczarek.jps.database.ast.unary.BagExpression;

public class ExpressionTest {

	public static void main(String[] args) {
		Expression expression1 = new DotExpression(
			new DotExpression(
				new WhereExpression(
					new NameTerminal("osoba"),
					new NameTerminal("żonaty")
				),
				new NameTerminal("książka")
			),
			new NameTerminal("autor")
		);
		
		System.out.println(expression1);
		
		Expression expression2 = new WhereExpression(
			new NameTerminal("Firma"),
			new GreaterExpression(
				new AvgExpression(
					new DotExpression(
						new NameTerminal("zatrudnia"), 
						new NameTerminal("pensja")
					)
				),
				new DoubleTerminal(2550.50)
			)
		);
		
		System.out.println(expression2);
		
		Expression expression3 = new WhereExpression(
			new NameTerminal("Pracownik"),
			new InExpression(
				new DotExpression(
					new NameTerminal("adres"),
					new NameTerminal("miasto")
				),
				new BagExpression(
					new ComaExpression(
						new StringTerminal("Warszawa"),
						new StringTerminal("Łódź")
					)
				)
			)
		);
		
		System.out.println(expression3);
		
		Expression expression4 = new InExpression(
			new BagExpression(
				new ComaExpression(
					new IntegerTerminal(1),
					new AddExpression(
						new IntegerTerminal(2),
						new IntegerTerminal(1)
					)
				)
			),
			new BagExpression(
				new ComaExpression(
					new AddExpression(
						new IntegerTerminal(4),
						new IntegerTerminal(1)
					),
					new AddExpression(
						new IntegerTerminal(3),
						new IntegerTerminal(2)
					)
				)
			)
		);
		
		System.out.println(expression4);
		
		Expression expression5 = new DotExpression(
			new WhereExpression(
				new NameTerminal("Pracownik"),
				new EqualExpression(
					new NameTerminal("nazwisko"),
					new StringTerminal("Kowalski")
				)
			),
			new WhereExpression(
				new NameTerminal("adres"),
				new EqualExpression(
					new NameTerminal("miasto"),
					new StringTerminal("Łódź")
				) 
			)
		);
		
		System.out.println(expression5);
	}

}
