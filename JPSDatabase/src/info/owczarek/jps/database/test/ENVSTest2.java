package info.owczarek.jps.database.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import info.owczarek.jps.database.envs.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;

public class ENVSTest2 {

	// ((emp where exists address) where address.number>20).(street, city)
	// Jak dla mnie to to zapytanie jest błędne, ale program poniżej robi co powinien
	// 1. Na początku wybieramy wszystkich pracowników, którzy mają adresy
	// 2. Z tych pracowników wybieramy wszystkich, którzy w polu address mają number większy od 20
	// 3. Z każdego z tych pracowników pobieramy pole street i city. Bezpośrednio w żadnym z pracowników
	// ich nie ma, więc w sumie dostajemy pusty bag
	public static void main(String[] args) {
		SBAStore sbaStore = new SBAStoreImpl();
		OID rootOid = sbaStore.loadXML("dataForEnvsTest.xml");
		System.out.println(sbaStore);

		ENVS envs = new ENVS();
		envs.init(rootOid, sbaStore);

		QResStack qres = new QResStack();

		// (emp where exists address)
		{
			// 1. Zainicjować whereResult = bag()
			BagResult whereResult = new BagResult();

			// 2. Wykonać eval(q1) i zrobić q1res = QRES.POP()
			BagResult evalQ1 = envs.bind("emp");
			qres.push(evalQ1);
			BagResult q1Result = (BagResult) qres.pop();

			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for (AbstractQueryResult x : q1Result.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);

				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(x, sbaStore);
				frame.add(frameBinders);

				// 3.3.Wykonać eval(q2)
				// exists address
				ReferenceResult empReference = (ReferenceResult) x;
				ComplexObject empObject = (ComplexObject) sbaStore.retrieve(empReference.getOIDValue());
				List<OID> empObjectChildrenOids = empObject.getChildrenOids();
				boolean exists = false;
				for (OID empObjectChildOid : empObjectChildrenOids) {
					SBAObject empObjectChild = sbaStore.retrieve(empObjectChildOid);
					if (StringUtils.equals("address", empObjectChild.getName())) {
						exists = true;
						break;
					}
				}

				BooleanResult evalq2;
				if (exists) {
					evalq2 = new BooleanResult(true);
				} else {
					evalq2 = new BooleanResult(false);
				}
				qres.push(evalq2);

				// 3.3 [...] i zrobić q2res = QRES.POP()
				BooleanResult q2Res = (BooleanResult) qres.pop();

				// 3.4. Jeżeli q2res wynosi true dodać x do whereres
				if (q2Res.getValue() == true) {
					whereResult.addElement(x);
				}

				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}

			// 4. Zrobić QRES.PUSH(whereres)
			qres.push(whereResult);
		}

		// ((emp where exists address) where address.number>20)
		{
			// 1. Zainicjować whereResult = bag()
			BagResult whereResult = new BagResult();

			// 2. Wykonać eval(q1) [...] - już zrobione

			// 2. [...] i zrobić q1res = QRES.POP()
			BagResult q1Result = (BagResult) qres.pop();

			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for (AbstractQueryResult empReference : q1Result.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);

				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(empReference, sbaStore);
				frame.add(frameBinders);

				// 3.3.Wykonać eval(q2)
				// address.number > 20
				{
					// address.number
					{
						// 1. Zainicjalizować dotres = bag()
						BagResult dotResult = new BagResult();

						// 2. Wykonać eval(q1) [...]
						BagResult evalq1 = envs.bind("address");
						qres.push(evalq1);

						// 2. [...] i zrobić q1res = QRES.POP()
						BagResult q1res = (BagResult) qres.pop();

						// 3. Dla każdego elementu x ze zbioru q1res wykonać:
						for (AbstractQueryResult addressReference : q1res.getElements()) {
							// 3.1.Utworzyć nową sekcję na ENVS
							ENVSFrame addressFrame = new ENVSFrame();
							envs.push(addressFrame);

							// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
							List<ENVSBinder> addressBinders = envs.nested(addressReference, sbaStore);
							addressFrame.add(addressBinders);

							// 3.3.Wykonać eval(q2) [...]
							ReferenceResult evalQ2 = (ReferenceResult) envs.bind("number").getElements().iterator()
									.next();

							qres.push(evalQ2);

							// 3.3 [...] i zrobić q2res = QRES.POP()
							ReferenceResult q2Res = (ReferenceResult) qres.pop();

							// 3.4.Dodać q2res do dotres (dotres ∪ q2res)
							dotResult.addElement(q2Res);

							// 3.5.Zamknąć sekcję ENVS
							envs.pop();
						}
						// 4. Zrobić QRES.PUSH(dotres)
						qres.push(dotResult);
					}

					// 20
					IntegerResult result20 = new IntegerResult(20);
					qres.push(result20);

					// >
					IntegerResult q2res = (IntegerResult) qres.pop();

					BagResult q1Bag = (BagResult) qres.pop();
					ReferenceResult q1Reference = (ReferenceResult) q1Bag.getElements().iterator().next();
					SimpleObject<Integer> q1resObject = (SimpleObject<Integer>) sbaStore.retrieve(q1Reference
							.getOIDValue());
					IntegerResult q1rresDereferences = new IntegerResult(q1resObject.getValue());

					Boolean greaterThanResult = q1rresDereferences.getValue() > q2res.getValue();

					BooleanResult evalGreaterThanResult = new BooleanResult(greaterThanResult);
					qres.push(evalGreaterThanResult);
				}

				// 3.3 [...] i zrobić q2res = QRES.POP()
				BooleanResult q2Res = (BooleanResult) qres.pop();

				// 3.4. Jeżeli q2res wynosi true dodać x do whereres
				if (q2Res.getValue() == true) {
					whereResult.addElement(empReference);
				}

				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}

			// 4. Zrobić QRES.PUSH(whereres)
			qres.push(whereResult);
		}

		// ((emp where exists address) where address.number>20).(street, city)
		{
			// 1. Zainicjalizować dotres = bag()
			BagResult dotResult = new BagResult();

			// 2. Wykonać eval(q1) i zrobić q1res = QRES.POP()
			BagResult q1res = (BagResult) qres.pop();

			// 3. Dla każdego elementu x ze zbioru q1res wykonać:
			for (AbstractQueryResult x : q1res.getElements()) {
				// 3.1.Utworzyć nową sekcję na ENVS
				ENVSFrame frame = new ENVSFrame();
				envs.push(frame);

				// 3.2.Wykonać nested(x). Wynik wprowadzić do sekcji utworzonej w poprzednim kroku
				List<ENVSBinder> frameBinders = envs.nested(x, sbaStore);
				frame.add(frameBinders);
				
				// 3.3.Wykonać eval(q2) [...]
				// street, city
				{
					// 1. Zainicjalizuj pusty bag (nazywany dalej eres).
					BagResult eres = new BagResult();
					
					// 2. Wykonaj oba podwyrażenia
					BagResult streetBag = envs.bind("street");
					BagResult cityBag = envs.bind("city");
					qres.push(streetBag);
					qres.push(cityBag);
					
					// 3. Podnieś jeden element z QRES (nazywany dalej e2res).
					// 4. Podnieś drugi element z QRES (nazywany dalej e1res).
					BagResult e2res = (BagResult) qres.pop();
					BagResult e1res = (BagResult) qres.pop();
					
					for (AbstractQueryResult e1resElement: e1res.getElements()) {
						List<AbstractQueryResult> structElements = new ArrayList<AbstractQueryResult>();
						structElements.add(e1resElement);
						for (AbstractQueryResult e2resElement: e2res.getElements()) {
							structElements.add(e2resElement);
						}
						
						StructResult struct = new StructResult(structElements);
						eres.addElement(struct);
					}
					qres.push(eres);
				}
				
				
				// 3.3 [...]  i zrobić q2res = QRES.POP()
				BagResult q2Res = (BagResult) qres.pop();
				
				// 3.4.Dodać q2res do dotres (dotres ∪ q2res)
				dotResult.addElement(q2Res);
				
				// 3.5.Zamknąć sekcję ENVS
				envs.pop();
			}
			// 4. Zrobić QRES.PUSH(dotres)
			qres.push(dotResult);
		}
		
		System.out.println(qres);
	}

}
