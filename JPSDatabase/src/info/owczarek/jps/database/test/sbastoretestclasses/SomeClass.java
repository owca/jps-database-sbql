package info.owczarek.jps.database.test.sbastoretestclasses;

public class SomeClass {
	public String exemplaryString = "exemplaryString";
	public int exemplaryInteger = 1;
	public double exemplaryDouble = 3.14;
	public boolean exemplaryBoolean = true;
}