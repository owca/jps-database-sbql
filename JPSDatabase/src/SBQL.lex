
package info.owczarek.jps.database.parser;

import java_cup.runtime.Symbol;
import static info.owczarek.jps.database.parser.SBQLSym.*;

%%

%{ 
	private StringBuffer str;

	private Symbol createToken(int id) {
		return createToken(id, yytext());
	}
        
	private Symbol createToken(int id, Object o) {
		return new Symbol(id, yyline, yycolumn, o);
	}

	public int getPos() {
		return zzMarkedPos;     
	}
%}

%eofval{
  return createToken(EOF);
%eofval}

%public
%class SBQLLexer
%cup
%line
%column

INTEGER = [-+]?[0-9]+
BOOLEAN = true|false
DOUBLE = [-+]?[0-9]+\.[0-9]+
STRING = [\"][^\"]*[\"]
LINE_END = \r|\n|\r\n 
WHITESPACE = {LINE_END} | [ \t\f]
IDENTIFIER = [_a-zA-Z][_0-9a-zA-Z]*

%%

"+"						{ return createToken(PLUS); }
"-"						{ return createToken(MINUS); }
"*"						{ return createToken(MULTIPLY); }
"/"						{ return createToken(DIVIDE); }
"%"						{ return createToken(MODULO); }
"=="					{ return createToken(EQUALS); }
"!="					{ return createToken(NOT_EQUALS); }
">"						{ return createToken(MORE); }
"<"						{ return createToken(LESS); }
">="					{ return createToken(MORE_OR_EQUAL); }
"<="					{ return createToken(LESS_OR_EQUAL); }
","						{ return createToken(COMA); }
"."						{ return createToken(DOT); }
"("						{ return createToken(LEFT_ROUND_BRACKET); }
")"						{ return createToken(RIGHT_ROUND_BRACKET); }
"OR"|"or"|"\|\|"		{ return createToken(OR); }
"AND"|"and"|"&&"		{ return createToken(AND); }
"XOR"|"xor"				{ return createToken(XOR); }
"NAND"|"nand"			{ return createToken(NAND); }
"SUM"|"sum"				{ return createToken(SUM); }
"AVG"|"avg"				{ return createToken(AVG); }
"UNIQUE"|"unique"		{ return createToken(UNIQUE); }
"UNION"|"union"			{ return createToken(UNION); }
"MIN"|"min"				{ return createToken(MIN); }
"MAX"|"max"				{ return createToken(MAX); }
"COUNT"|"count"			{ return createToken(COUNT); }
"AS"|"as"				{ return createToken(AS); }
"GROUP AS"|"group as"	{ return createToken(GROUP_AS); }
"BAG"|"bag"				{ return createToken(BAG); }
"IN"|"in"				{ return createToken(IN); }
"WHERE"|"where"			{ return createToken(WHERE); }
"EXISTS"|"exists" 		{ return createToken(EXISTS); }
"NOT"|"not"|"!"			{ return createToken(NOT); }
"STRUCT"|"struct"		{ return createToken(STRUCT); }
"INTERSECT"|"intersect"	{ return createToken(INTERSECT); }
"JOIN"|"join"			{ return createToken(JOIN); }
"MINUS"|"minus"			{ return createToken(MINUS_SET); }
"FORALL"|"forall"|"ALL"|"all"	{ return createToken(FORALL); }
"FORANY"|"forany"|"ANY"|"any"	{ return createToken(FORANY); }
"EMPTY"|"empty"			{ return createToken(EMPTY); }
"UNIQUE"|"unique"		{ return createToken(UNIQUE); }


{WHITESPACE} {}
{STRING}	{
				String text = yytext();
				text = text.substring(1, text.length() - 1);
				return createToken(STRING_LITERAL, text);
			}

{INTEGER}	{
				int val;
				try {
					val = Integer.parseInt(yytext());
				} catch (Exception e) {
					throw new RuntimeException(e.getMessage());
				}
				return createToken(INTEGER_LITERAL, new Integer(val));
			}

{DOUBLE}	{
				double val;
				try {
					val = Double.parseDouble(yytext());
				} catch (Exception e) {
					throw new RuntimeException(e.getMessage());
				}
				return createToken(DOUBLE_LITERAL, new Double(val));
			}
{BOOLEAN}	{
				boolean val;
				try {
					val = Boolean.parseBoolean(yytext());
				} catch (Exception e) {
					throw new RuntimeException(e.getMessage());
				}
				return createToken(BOOLEAN_LITERAL, new Boolean(val));
			}
			
{IDENTIFIER} { return createToken(IDENTIFIER_LITERAL, yytext()); }