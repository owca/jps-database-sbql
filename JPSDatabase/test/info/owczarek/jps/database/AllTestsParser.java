package info.owczarek.jps.database;

import info.owczarek.jps.database.parser.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AggregationTests.class, ArithmeticTests.class, ComparisonTests.class, DotAndWhereTests.class,
		LogicTests.class, NameTests.class, SetTests.class, StructComaBagTests.class, TerminalExpressionsTest.class })
public class AllTestsParser {

}
