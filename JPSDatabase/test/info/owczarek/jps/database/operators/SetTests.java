package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;

import java.util.Collection;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.auxiliary.GroupAsExpression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class SetTests extends ExpressionsTest {

	// bag(1, 2, 3) intersect bag(2, 3) -> bag(2, 3)
	@Test
	public void bagOneTwoThreeIntersectBagTwoThree() {
		Expression expression = new IntersectExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))), new BagExpression(
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(2, 3)", bagResult.toString());
	}

	// 1 intersect 1 -> 1
	@Test
	public void oneIntersectOne() {
		Expression expression = new IntersectExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// (1, 2) intersect (2, 3) -> bag()
	@Test
	public void oneComaTwoIntersectTwoComaThree() {
		Expression expression = new IntersectExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(
				2)), new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// (1, 2) intersect (1, 2) -> struct(1, 2)
	@Test
	public void oneComaTwoIntersectOneComaTwo() {
		Expression expression = new IntersectExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(
				2)), new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult structResult = (StructResult) result;

		Collection<AbstractQueryResult> bagElements = structResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("struct(1, 2)", structResult.toString());
	}

	// bag("ala", 2, 3) intersect bag(2, 3.40) -> 2
	@Test
	public void bagAlaComaTwoComaThreeIntersectBagTwoComaThreePointFourty() {
		Expression expression = new IntersectExpression(new BagExpression(new ComaExpression(new StringTerminal("ala"),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))), new BagExpression(
				new ComaExpression(new IntegerTerminal(2), new DoubleTerminal(3.40))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(2, (int) integerResult.getValue());

		assertEquals("2", integerResult.toString());
	}

	// 1 union 2 -> bag(1, 2)
	@Test
	public void oneUnionTwo() {
		Expression expression = new UnionExpression(new IntegerTerminal(1), new IntegerTerminal(2));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(1, 2)", bagResult.toString());
	}

	// bag(1, 2) union bag(3, 4) -> bag(1, 2, 3, 4)
	@Test
	public void bagOneComaTwoUnionBagThreeComaFour() {
		Expression expression = new UnionExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new BagExpression(new ComaExpression(new IntegerTerminal(3),
				new IntegerTerminal(4))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(4, (int) bagElements.size());

		assertEquals("bag(1, 2, 3, 4)", bagResult.toString());
	}

	// (1, 2) union (3, 4)
	@Test
	public void oneComaTwoUnionThreeComaFour() {
		Expression expression = new UnionExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)),
				new ComaExpression(new IntegerTerminal(3), new IntegerTerminal(4)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(struct(1, 2), struct(3, 4))", bagResult.toString());
	}

	// bag(1, 2, 3) minus bag(2, 3) -> 1
	@Test
	public void bagOneTwoThreeMinusBagTwoThree() {
		Expression expression = new MinusExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))), new BagExpression(
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// 1 minus 1 -> bag()
	@Test
	public void oneMinusOne() {
		Expression expression = new MinusExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// (1, 2) minus (2, 3) -> struct(1, 2)
	@Test
	public void oneComaTwoMinusTwoComaThree() {
		Expression expression = new MinusExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult structResult = (StructResult) result;

		Collection<AbstractQueryResult> structElements = structResult.getElements();

		assertEquals(2, (int) structElements.size());

		assertEquals("struct(1, 2)", structResult.toString());
	}

	// (1, 2) minus (1, 2) -> bag()
	@Test
	public void oneComaTwoMinusOneComaTwo() {
		Expression expression = new MinusExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)),
				new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// bag("ala", 2, 3) minus bag(2, 3.40) -> bag("ala", 3)
	@Test
	public void bagAlaComaTwoComaThreeMinusBagTwoComaThreePointFourty() {
		Expression expression = new MinusExpression(new BagExpression(new ComaExpression(new StringTerminal("ala"),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))), new BagExpression(
				new ComaExpression(new IntegerTerminal(2), new DoubleTerminal(3.40))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(\"ala\", 3)", bagResult.toString());
	}

	// bag(booleanValue, 3) minus bag(booleanValue) -> 3
	@Test
	public void bagBooleanValueComaThreeMinusBagBooleanValue() {
		Expression expression = new MinusExpression(new BagExpression(new ComaExpression(new NameTerminal(
				"booleanValue"), new IntegerTerminal(3))), new BagExpression(new NameTerminal("booleanValue")));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(3, (int) integerResult.getValue());

		assertEquals("3", integerResult.toString());
	}

	// forall 1 true
	@Test
	public void forallOnetrue() {
		Expression expression = new ForallExpression(new IntegerTerminal(1), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forall (bag(1, bag(2, 3) group as wew) as num) (num == 2) -> false
	@Test
	public void forallBagOneBagTwoThreeGroupAsWewAsNumNumEqualsTwo() {
		Expression expression = new ForallExpression(new AsExpression("num", new BagExpression(new ComaExpression(
				new IntegerTerminal(1), new GroupAsExpression("wew", new BagExpression(new ComaExpression(
						new IntegerTerminal(2), new IntegerTerminal(3))))))), new EqualExpression(new NameTerminal(
				"num"), new IntegerTerminal(2)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// forall (bag(1, bag(2, 3) group as wew) as num) (num == 1) -> Exception
	@Test(expected = RuntimeException.class)
	public void forallBagOneBagTwoThreeGroupAsWewAsNumNumEqualsOne() {
		Expression expression = new ForallExpression(new AsExpression("num", new BagExpression(new ComaExpression(
				new IntegerTerminal(1), new GroupAsExpression("wew", new BagExpression(new ComaExpression(
						new IntegerTerminal(2), new IntegerTerminal(3))))))), new EqualExpression(new NameTerminal(
				"num"), new IntegerTerminal(1)));
		expression.accept(visitor);
	}

	// forall emp married -> true
	@Test
	public void forallEmpMarried() {
		Expression expression = new ForallExpression(new NameTerminal("emp"), new NameTerminal("married"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany 1 true
	@Test
	public void foranyOnetrue() {
		Expression expression = new ForanyExpression(new IntegerTerminal(1), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany (bag(1, bag(2, 3) group as wew) as num) (num == 2) -> Exception
	@Test(expected = RuntimeException.class)
	public void foranyBagOneBagTwoThreeGroupAsWewAsNumNumEqualsTwo() {
		Expression expression = new ForanyExpression(new AsExpression("num", new BagExpression(new ComaExpression(
				new IntegerTerminal(1), new GroupAsExpression("wew", new BagExpression(new ComaExpression(
						new IntegerTerminal(2), new IntegerTerminal(3))))))), new EqualExpression(new NameTerminal(
				"num"), new IntegerTerminal(2)));
		expression.accept(visitor);
	}

	// forany (bag(1, bag(2, 3) group as wew) as num) (num == 1) -> true
	@Test
	public void foranyBagOneBagTwoThreeGroupAsWewAsNumNumEqualsOne() {
		Expression expression = new ForanyExpression(new AsExpression("num", new BagExpression(new ComaExpression(
				new IntegerTerminal(1), new GroupAsExpression("wew", new BagExpression(new ComaExpression(
						new IntegerTerminal(2), new IntegerTerminal(3))))))), new EqualExpression(new NameTerminal(
				"num"), new IntegerTerminal(1)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany emp married
	@Test
	public void foranyEmpMarried() {
		Expression expression = new ForanyExpression(new NameTerminal("emp"), new NameTerminal("married"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// unique(bag(1, 2, 1)) -> bag(1, 2)
	@Test
	public void uniqueBagOneTwoOne() {
		Expression expression = new UniqueExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(1)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		assertEquals(2, bagResult.getElements().size());

		assertEquals("bag(1, 2)", bagResult.toString());
	}

	// unique(bag(1.01, 2, 1.01, "ala")) -> bag(1.01, 2, "ala")
	@Test
	public void uniqueBagOnePointZeroOneTwoOnePointZeroOneAla() {
		Expression expression = new UniqueExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new IntegerTerminal(2), new ComaExpression(new DoubleTerminal(1.01),
						new StringTerminal("ala"))))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		assertEquals(3, bagResult.getElements().size());

		assertEquals("bag(1.01, 2, \"ala\")", bagResult.toString());
	}

	// empty(1) -> false
	@Test
	public void emptyOne() {
		Expression expression = new EmptyExpression(new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// empty(bag(1.01, 2.35, 3))
	@Test
	public void emptyBagOnePointZeroOneTwoPointThirtyFiveThree() {
		Expression expression = new EmptyExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new DoubleTerminal(2.35), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// empty(1 where false) -> true
	@Test
	public void emptyOneWhereFalse() {
		Expression expression = new EmptyExpression(new WhereExpression(new IntegerTerminal(1), new BooleanTerminal(
				false)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// bag(2, 3) in bag(1, 2, 3) -> true
	@Test
	public void bagTwoThreeInBagOneTwoThree() {
		Expression expression = new InExpression(new BagExpression(new ComaExpression(new IntegerTerminal(2),
				new IntegerTerminal(3))), new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// 1 in 1 -> true
	@Test
	public void oneInOne() {
		Expression expression = new InExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// (1, 2) in (2, 3)
	@Test
	public void oneTwoInTwoThree() {
		Expression expression = new InExpression(new StructExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new StructExpression(new ComaExpression(new IntegerTerminal(2),
				new IntegerTerminal(3))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// (1, 2) in (1, 2)
	@Test
	public void oneTwoInOneTwo() {
		Expression expression = new InExpression(new StructExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new StructExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}
}
