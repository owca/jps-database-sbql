package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;

import org.junit.Test;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

public class AggregationTests extends ExpressionsTest {

	// max(1)
	@Test
	public void maxFromOne() {
		Expression expression = new MaxExpression(new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// max(bag(1, 3.35, 3))
	@Test
	public void maxFromOneThreePointThirtyFiveAndThree() {
		Expression expression = new MaxExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new ComaExpression(new DoubleTerminal(3.35), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(3.35, (double) doubleResult.getValue(), 0.001);

		assertEquals("3.35", doubleResult.toString());
	}

	// max(bag(-1, -3.35, -3))
	@Test
	public void maxFromMinusOneThreePointThirtyMinusFiveAndMinusThree() {
		Expression expression = new MaxExpression(new BagExpression(new ComaExpression(new IntegerTerminal(-1),
				new ComaExpression(new DoubleTerminal(-3.35), new IntegerTerminal(-3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(-1, (int) integerResult.getValue(), 0.001);

		assertEquals("-1", integerResult.toString());
	}

	// min(1)
	@Test
	public void minFromOne() {
		Expression expression = new MinExpression(new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// min(bag(1.01, 2.35, 3))
	@Test
	public void minFromOnePointZeroOneTwoPointThirtyFiveAndThree() {
		Expression expression = new MinExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new DoubleTerminal(2.35), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(1.01, (double) doubleResult.getValue(), 0.001);

		assertEquals("1.01", doubleResult.toString());
	}

	// min(bag(1.01, -10.0, 3))
	@Test
	public void minFromOnePointZeroOneMinusTenAndThree() {
		Expression expression = new MinExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new DoubleTerminal(-10.0), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(-10, (int) integerResult.getValue());

		assertEquals("-10", integerResult.toString());
	}

	// sum(1)
	@Test
	public void sumFromOne() {
		Expression expression = new SumExpression(new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// min(bag(1.01, 2.35, 3))
	@Test
	public void sumFromOnePointZeroOneTwoPointThirtyFiveAndThree() {
		Expression expression = new MinExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new DoubleTerminal(2.35), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(1.01, (double) doubleResult.getValue(), 0.001);

		assertEquals("1.01", doubleResult.toString());
	}

	// count(1)
	@Test
	public void countFromOne() {
		Expression expression = new CountExpression(new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// count(bag(1.01, 2.35, 3))
	@Test
	public void countFromBagOnePointZeroOneTwoPointThirtyFiveAndThree() {
		Expression expression = new CountExpression(new BagExpression(new ComaExpression(new DoubleTerminal(1.01),
				new ComaExpression(new DoubleTerminal(2.35), new IntegerTerminal(3)))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(3, (int) integerResult.getValue());

		assertEquals("3", integerResult.toString());
	}

	// count(1.01, 2.35, 3)
	@Test
	public void countFromOnePointZeroOneTwoPointThirtyFiveAndThree() {
		Expression expression = new CountExpression(new ComaExpression(new DoubleTerminal(1.01), new ComaExpression(
				new DoubleTerminal(2.35), new IntegerTerminal(3))));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

}
