package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;

import java.util.List;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.comparison.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class ComparisonTests extends ExpressionsTest {

	// integerNumber == 10 -> true
	@Test
	public void integerNumberEqualsToTen() {
		Expression expression = new EqualExpression(new NameTerminal("integerNumber"), new IntegerTerminal(10));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// bag(1,2) == 2 -> exception
	@Test(expected = RuntimeException.class)
	public void bagOneTwoEqualsToTwo() {
		Expression expression = new EqualExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new IntegerTerminal(2));
		expression.accept(visitor);
		fail();
	}

	// booleanValue == true -> true
	@Test
	public void booleanValueEqualsToTrue() {
		Expression expression = new EqualExpression(new NameTerminal("booleanValue"), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// stringValue == "Ala" -> true
	@Test
	public void stringValueEqualsToAla() {
		Expression expression = new EqualExpression(new NameTerminal("stringValue"), new StringTerminal("Ala"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 == 2 -> false
	@Test
	public void oneEqualsToTwo() {
		Expression expression = new EqualExpression(new IntegerTerminal(1), new IntegerTerminal(2));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 1 == true -> false
	@Test
	public void oneEqualsToTrueTest() {
		Expression expression = new EqualExpression(new IntegerTerminal(1), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 5 == "5" -> false
	@Test
	public void fiveEqualsToStringFiveTest() {
		Expression expression = new EqualExpression(new IntegerTerminal(1), new StringTerminal("5"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 5.50 == 5 -> false
	@Test
	public void fivePointFiftyEqualsToFiveTest() {
		Expression expression = new EqualExpression(new DoubleTerminal(5.50), new IntegerTerminal(5));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 1 > 1 -> false
	@Test
	public void oneGreaterThanOne() {
		Expression expression = new GreaterExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 3 > 2.99 -> true
	@Test
	public void threeGreaterThanTwoPointNinetyNine() {
		Expression expression = new GreaterExpression(new IntegerTerminal(3), new DoubleTerminal(2.99));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.35 > 24.34 -> true
	@Test
	public void twentyFourPointThirtyFiveGreaterThanTwentyFourPointThirtyFour() {
		Expression expression = new GreaterExpression(new DoubleTerminal(24.35), new DoubleTerminal(24.34));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 >= 1 -> true
	@Test
	public void oneGreaterOrEqualToOne() {
		Expression expression = new GreaterOrEqualExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 3 >= 2.99 -> true
	@Test
	public void threeGreaterOrEqualThanTwoPointNinetyNine() {
		Expression expression = new GreaterOrEqualExpression(new IntegerTerminal(3), new DoubleTerminal(2.99));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.35 >= 24.34 -> true
	@Test
	public void twentyFourPointThirtyFiveGreaterOrEqualThanTwentyFourPointThirtyFour() {
		Expression expression = new GreaterOrEqualExpression(new DoubleTerminal(24.35), new DoubleTerminal(24.34));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 <= 1 -> true
	@Test
	public void oneLesserOrEqualToOne() {
		Expression expression = new LesserOrEqualExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.34 <= 24.35 -> true
	@Test
	public void twentyFourPointThirtyFourLesserOrEqualToTwentyFourPointThirtyFive() {
		Expression expression = new LesserOrEqualExpression(new DoubleTerminal(24.34), new DoubleTerminal(24.35));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 2.99 <= 3 -> true
	@Test
	public void twoPointNinetyNineLesserOrEqualToThree() {
		Expression expression = new LesserOrEqualExpression(new DoubleTerminal(2.99), new IntegerTerminal(3));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 < 1 -> false
	@Test
	public void oneLesserThanOne() {
		Expression expression = new LesserExpression(new IntegerTerminal(1), new IntegerTerminal(1));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 24.34 < 24.35 -> true
	@Test
	public void twentyFourPointThirtyFourLesserThanTwentyFourPointThirtyFive() {
		Expression expression = new LesserExpression(new DoubleTerminal(24.34), new DoubleTerminal(24.35));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 2.99 < 3 -> true
	@Test
	public void twoPointNinetyNineLesserThanThree() {
		Expression expression = new LesserExpression(new DoubleTerminal(2.99), new IntegerTerminal(3));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

}
