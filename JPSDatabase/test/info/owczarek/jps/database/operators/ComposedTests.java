package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;
import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.auxiliary.GroupAsExpression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.arithmetic.AddExpression;
import info.owczarek.jps.database.ast.binary.arithmetic.DivideExpression;
import info.owczarek.jps.database.ast.binary.arithmetic.ModuloExpression;
import info.owczarek.jps.database.ast.binary.arithmetic.MultiplyExpression;
import info.owczarek.jps.database.ast.binary.arithmetic.SubtractExpression;
import info.owczarek.jps.database.ast.binary.comparison.*;
import info.owczarek.jps.database.ast.binary.logic.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class ComposedTests extends ExpressionsTest {

	// Test wykorzystujący wszystkie operatory w ramach zadania 5 i 6. W zasadzie nie testuje on nic rozsądnego, a tylko
	// wykorzystuje wymagane operatory.
	@Test
	public void composedTest() {

		MinusExpression minusExpression = new MinusExpression(new BagExpression(new ComaExpression(new IntegerTerminal(
				1), new ComaExpression(new IntegerTerminal(2), new ComaExpression(new IntegerTerminal(3),
				new IntegerTerminal(4))))), new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2)))); // wykorzystane

		WhereExpression whereExpression = new WhereExpression(new BagExpression(new ComaExpression(new IntegerTerminal(
				1), new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3)))), new AndExpression(
				new EqualExpression(new IntegerTerminal(1), new IntegerTerminal(1)), new NotEqualExpression(
						new IntegerTerminal(2), new IntegerTerminal(8)))); // wykorzystane

		IntersectExpression intersectExpression = new IntersectExpression(new BagExpression(minusExpression),
				new BagExpression(new IntegerTerminal(3))); // wykorzystane

		ForallExpression forall = new ForallExpression(new AsExpression("n", intersectExpression), new AndExpression(
				new GreaterExpression(new NameTerminal("n"), new IntegerTerminal(3)), new LesserExpression(
						new NameTerminal("n"), new ModuloExpression(new IntegerTerminal(55), new IntegerTerminal(10))))); // wykorzystane

		ForanyExpression foranyExpression = new ForanyExpression(new IntegerTerminal(4), new OrExpression(
				new GreaterOrEqualExpression(new IntegerTerminal(3), new SubtractExpression(new DoubleTerminal(9.1),
						new DoubleTerminal(1.1))), new LesserOrEqualExpression(new DoubleTerminal(2.2),
						new IntegerTerminal(8)))); // wykorzystane

		BagExpression someBag = new BagExpression(new ComaExpression(new DivideExpression(new DoubleTerminal(16.0),
				new DoubleTerminal(4.0)), new IntegerTerminal(3))); // wykorzystane

		ExistsExpression existsExpression = new ExistsExpression(new JoinExpression(whereExpression, someBag)); // wykorzystane

		AddExpression addExpression = new AddExpression(new MinExpression(someBag), new AddExpression(
				new MaxExpression(someBag), new AddExpression(new SumExpression(new UnionExpression(someBag, someBag)),
						new MultiplyExpression(new AvgExpression(new UniqueExpression(someBag)), new CountExpression(
								someBag))))); // wykorzystane

		InExpression inExpression = new InExpression(new IntegerTerminal(2), addExpression); // wykorzystane

		ExistsExpression existsExpression2 = new ExistsExpression(new GroupAsExpression("n", new DotExpression(
				new NameTerminal("emp"), new NameTerminal("married")))); // wykorzystane

		Expression expression = new AndExpression(existsExpression, new XorExpression(forall, new AndExpression(
				foranyExpression, new OrExpression(inExpression, new NotExpression(existsExpression2)))));

		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

	}
}
