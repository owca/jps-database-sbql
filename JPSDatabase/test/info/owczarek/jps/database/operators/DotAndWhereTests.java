package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAObject;
import info.owczarek.jps.database.store.SimpleObject;

public class DotAndWhereTests extends ExpressionsTest {

	// (1 as x).x -> 1
	@Test
	public void oneAsXDotX() {
		Expression expression = new DotExpression(new AsExpression("x", new IntegerTerminal(1)), new NameTerminal("x"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// (1, 2).("Ala") -> "Ala"
	@Test
	public void oneComaTwoDotAla() {
		Expression expression = new DotExpression(new ComaExpression(new IntegerTerminal(1), new IntegerTerminal(2)),
				new StringTerminal("Ala"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StringResult);

		StringResult stringResult = (StringResult) result;

		assertEquals("Ala", stringResult.getValue());
	}

	// emp.book.author -> bag(ref(), ref(), ref())
	@Test
	public void empBookAuthor() {
		Expression expression = new DotExpression(new NameTerminal("emp"), new DotExpression(new NameTerminal("book"),
				new NameTerminal("author")));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(3, bagElements.size());

		for (AbstractQueryResult bagElement : bagElements) {
			assertTrue(bagElement instanceof ReferenceResult);

			ReferenceResult elementReference = (ReferenceResult) bagElement;
			OID referencedElementOID = elementReference.getOIDValue();
			SBAObject sbaObject = sbaStore.retrieve(referencedElementOID);

			assertTrue(sbaObject instanceof SimpleObject<?>);

			SimpleObject<String> stringObject = (SimpleObject<String>) sbaObject;
			String objectValue = stringObject.getValue();

			assertTrue("Juliusz Słowacki".equals(objectValue) || "Adam Mickiewicz".equals(objectValue)
					|| "Aleksander Dumas (syn)".equals(objectValue));
		}
	}

	// bag(1, 2).("Ala") -> bag("Ala", "Ala")
	@Test
	public void bagOneComaTwoDotAla() {
		Expression expression = new DotExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new StringTerminal("Ala"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, bagElements.size());

		assertEquals("bag(\"Ala\", \"Ala\")", result.toString());
	}

	// bag(1, 2) where true -> bag(1, 2)
	@Test
	public void bagOneComaTwoWhereTrue() {
		Expression expression = new WhereExpression(new BagExpression(new ComaExpression(new IntegerTerminal(1),
				new IntegerTerminal(2))), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, bagElements.size());

		assertEquals("bag(1, 2)", result.toString());
	}

	// bag(1, 2, 3) as n where n == 1 -> <n, 1>
	@Test
	public void bagOneTwoThreeAsNWhereNEqualsOne() {
		Expression expression = new WhereExpression(new AsExpression("n", new BagExpression(new ComaExpression(
				new IntegerTerminal(1), new ComaExpression(new IntegerTerminal(2), new IntegerTerminal(3))))),
				new EqualExpression(new NameTerminal("n"), new IntegerTerminal(1)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		assertEquals("<n, 1>", result.toString());
	}

	// emp where married -> bag(ref(), ref())
	@Test
	public void empWhereMarried() {
		Expression expression = new WhereExpression(new NameTerminal("emp"), new NameTerminal("married"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, bagElements.size());

		for (AbstractQueryResult bagElement : bagElements) {
			assertTrue(bagElement instanceof ReferenceResult);
		}
	}
}
