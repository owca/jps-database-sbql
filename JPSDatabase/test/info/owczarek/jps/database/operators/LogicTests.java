package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;
import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.logic.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.ast.unary.NotExpression;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class LogicTests extends ExpressionsTest {

	// true and false -> false
	@Test
	public void trueAndFalse() {
		Expression expression = new AndExpression(new BooleanTerminal(true), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// booleanValue and true -> true
	@Test
	public void booleanValueAndTrue() {
		Expression expression = new AndExpression(new NameTerminal("booleanValue"), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// false and true and 1 -> Exception
	@Test(expected = RuntimeException.class)
	public void falseAndTrueAndOne() {
		Expression expression = new AndExpression(new BooleanTerminal(false), new AndExpression(new BooleanTerminal(
				true), new IntegerTerminal(1)));
		expression.accept(visitor);
	}

	// true or false -> true
	@Test
	public void trueOrFalse() {
		Expression expression = new OrExpression(new BooleanTerminal(true), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue or false -> true
	@Test
	public void booleanValueOrFalse() {
		Expression expression = new OrExpression(new NameTerminal("booleanValue"), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// false or true or 1 -> Exception
	@Test(expected = RuntimeException.class)
	public void falseOrTrueAndOne() {
		Expression expression = new OrExpression(new BooleanTerminal(false), new OrExpression(
				new BooleanTerminal(true), new IntegerTerminal(1)));
		expression.accept(visitor);
	}

	// not true -> false
	@Test
	public void notTrue() {
		Expression expression = new NotExpression(new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// not false -> true
	@Test
	public void notFalse() {
		Expression expression = new NotExpression(new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// not booleanValue -> false
	@Test
	public void notBooleanValue() {
		Expression expression = new NotExpression(new NameTerminal("booleanValue"));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// not bag(false) -> true
	@Test
	public void botBagWithFalse() {
		Expression expression = new NotExpression(new BagExpression(new BooleanTerminal(false)));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// true xor false -> true
	@Test
	public void trueXorFalse() {
		Expression expression = new XorExpression(new BooleanTerminal(true), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// true xor true -> false
	@Test
	public void trueXorTrue() {
		Expression expression = new XorExpression(new BooleanTerminal(true), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// false xor true -> true
	@Test
	public void falseXorTrue() {
		Expression expression = new XorExpression(new BooleanTerminal(false), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue xor true -> false
	@Test
	public void booleanValueXorFalse() {
		Expression expression = new XorExpression(new NameTerminal("booleanValue"), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// true nand false -> true
	@Test
	public void trueNandFalse() {
		Expression expression = new NandExpression(new BooleanTerminal(true), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// false nand true -> true
	@Test
	public void falseNandTrue() {
		Expression expression = new NandExpression(new BooleanTerminal(false), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// true nand true -> false
	@Test
	public void trueNandTrue() {
		Expression expression = new NandExpression(new BooleanTerminal(true), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// false nand false -> true
	@Test
	public void falseNandFalse() {
		Expression expression = new NandExpression(new BooleanTerminal(false), new BooleanTerminal(false));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue nand true -> false
	@Test
	public void booleanValuenandFalse() {
		Expression expression = new NandExpression(new NameTerminal("booleanValue"), new BooleanTerminal(true));
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}
}
