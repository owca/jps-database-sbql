package info.owczarek.jps.database.operators;

import static org.junit.Assert.*;
import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.OID;
import info.owczarek.jps.database.store.SBAObject;
import info.owczarek.jps.database.store.SimpleObject;

import org.junit.Test;

public class TerminalExpressionsTest extends ExpressionsTest {

	@Test
	public void BooleanTest() {
		Expression expression = new BooleanTerminal(true);
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	@Test
	public void DoubleTest() {
		Expression expression = new DoubleTerminal(4.2);
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(4.2, doubleResult.getValue(), 0.001);
	}

	@Test
	public void IntegerTest() {
		Expression expression = new IntegerTerminal(7);
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(7, (int) integerResult.getValue());
	}

	// booleanValue -> true
	@Test
	public void NameTest() {
		Expression expression = new NameTerminal("booleanValue");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult objectReference = (ReferenceResult) result;
		OID referencedObjectOid = objectReference.getOIDValue();
		SBAObject referencedObject = sbaStore.retrieve(referencedObjectOid);

		assertTrue(referencedObject instanceof SimpleObject<?>);
		SimpleObject<Boolean> referencedBoolean = (SimpleObject<Boolean>) referencedObject;

		assertEquals(true, referencedBoolean.getValue());
	}

}
