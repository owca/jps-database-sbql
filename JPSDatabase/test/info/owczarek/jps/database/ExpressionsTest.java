package info.owczarek.jps.database;

import info.owczarek.jps.database.ast.visitor.ASTVisitor;
import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.envs.*;
import info.owczarek.jps.database.parser.SBQLCup;
import info.owczarek.jps.database.qres.*;
import info.owczarek.jps.database.store.*;
import org.junit.Before;

public abstract class ExpressionsTest {
	protected static ENVS envs;
	protected static SBAStore sbaStore;
	protected static QResStack qres;
	protected static ASTVisitor visitor;
	private static boolean sbaHasBeenPrinted = false;

	@Before
	public void setUpBefore() throws Exception {
		OID.restart();
		envs = new ENVS();
		sbaStore = new SBAStoreImpl();
		qres = new QResStack();
		OID rootOid = sbaStore.loadXML("dane_do_zap_testowych.xml");
		envs.init(rootOid, sbaStore);
		visitor = new ASTVisitor(sbaStore, qres, envs);
		if (!sbaHasBeenPrinted) {
			System.out.println(sbaStore);
			sbaHasBeenPrinted = true;
		}
	}

	public Expression createASTFromExpression(String expression) {
		SBQLCup parser = new SBQLCup(expression);
		try {
			parser.user_init();
			Expression result = (Expression) parser.parse().value;
			return result;
		} catch (Exception e) {
			throw new RuntimeException("Error while creating AST from " + expression + ":" + e.getMessage());
		}
	}
}
