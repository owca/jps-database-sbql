package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;
import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.logic.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.ast.unary.NotExpression;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class LogicTests extends ExpressionsTest {

	// true and false -> false
	@Test
	public void trueAndFalse() {
		Expression expression = createASTFromExpression("true and false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// booleanValue and true -> true
	@Test
	public void booleanValueAndTrue() {
		Expression expression = createASTFromExpression("booleanValue and true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// false and true and 1 -> Exception
	@Test(expected = RuntimeException.class)
	public void falseAndTrueAndOne() {
		Expression expression = createASTFromExpression("false and true and 1");
		expression.accept(visitor);
	}

	// true or false -> true
	@Test
	public void trueOrFalse() {
		Expression expression = createASTFromExpression("true or false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue or false -> true
	@Test
	public void booleanValueOrFalse() {
		Expression expression = createASTFromExpression("booleanValue or false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// false or true or 1 -> Exception
	@Test(expected = RuntimeException.class)
	public void falseOrTrueAndOne() {
		Expression expression = createASTFromExpression("false or true or 1");
		expression.accept(visitor);
	}

	// not true -> false
	@Test
	public void notTrue() {
		Expression expression = createASTFromExpression("not true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// not false -> true
	@Test
	public void notFalse() {
		Expression expression = createASTFromExpression("not false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// not booleanValue -> false
	@Test
	public void notBooleanValue() {
		Expression expression = createASTFromExpression("not booleanValue");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// not bag(false) -> true
	@Test
	public void botBagWithFalse() {
		Expression expression = createASTFromExpression("not bag(false)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());
	}

	// true xor false -> true
	@Test
	public void trueXorFalse() {
		Expression expression = createASTFromExpression("true xor false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// true xor true -> false
	@Test
	public void trueXorTrue() {
		Expression expression = createASTFromExpression("true xor true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// false xor true -> true
	@Test
	public void falseXorTrue() {
		Expression expression = createASTFromExpression("false xor true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue xor true -> false
	@Test
	public void booleanValueXorFalse() {
		Expression expression = createASTFromExpression("booleanValue xor true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}

	// true nand false -> true
	@Test
	public void trueNandFalse() {
		Expression expression = createASTFromExpression("true nand false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// false xor true -> true
	@Test
	public void falseNandTrue() {
		Expression expression = createASTFromExpression("false nand true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// true nand true -> false
	@Test
	public void trueNandTrue() {
		Expression expression = createASTFromExpression("true nand true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// false nand false -> true
	@Test
	public void falseNandFalse() {
		Expression expression = createASTFromExpression("false nand false");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// booleanValue nand true -> false
	@Test
	public void booleanValuenandFalse() {
		Expression expression = createASTFromExpression("booleanValue nand true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());
	}
}
