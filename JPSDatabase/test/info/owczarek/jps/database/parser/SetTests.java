package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;

import java.util.Collection;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.auxiliary.GroupAsExpression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.comparison.EqualExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class SetTests extends ExpressionsTest {

	// bag(1, 2, 3) intersect bag(2, 3) -> bag(2, 3)
	@Test
	public void bagOneTwoThreeIntersectBagTwoThree() {
		Expression expression = createASTFromExpression("bag(1, 2, 3) intersect bag(2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(2, 3)", bagResult.toString());
	}

	// 1 intersect 1 -> 1
	@Test
	public void oneIntersectOne() {
		Expression expression = createASTFromExpression("1 intersect 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// (1, 2) intersect (2, 3) -> bag()
	@Test
	public void oneComaTwoIntersectTwoComaThree() {
		Expression expression = createASTFromExpression("(1, 2) intersect (2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// (1, 2) intersect (1, 2) -> struct(1, 2)
	@Test
	public void oneComaTwoIntersectOneComaTwo() {
		Expression expression = createASTFromExpression("(1, 2) intersect (1, 2)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult structResult = (StructResult) result;

		Collection<AbstractQueryResult> bagElements = structResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("struct(1, 2)", structResult.toString());
	}

	// bag("ala", 2, 3) intersect bag(2, 3.40) -> 2
	@Test
	public void bagAlaComaTwoComaThreeIntersectBagTwoComaThreePointFourty() {
		Expression expression = createASTFromExpression("bag(\"ala\", 2, 3) intersect bag(2, 3.40)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(2, (int) integerResult.getValue());

		assertEquals("2", integerResult.toString());
	}

	// 1 union 2 -> bag(1, 2)
	@Test
	public void oneUnionTwo() {
		Expression expression = createASTFromExpression("1 union 2");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(1, 2)", bagResult.toString());
	}

	// bag(1, 2) union bag(3, 4) -> bag(1, 2, 3, 4)
	@Test
	public void bagOneComaTwoUnionBagThreeComaFour() {
		Expression expression = createASTFromExpression("bag(1, 2) union bag(3, 4)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(4, (int) bagElements.size());

		assertEquals("bag(1, 2, 3, 4)", bagResult.toString());
	}

	// (1, 2) union (3, 4) -> bag(struct(1, 2), struct(3, 4))
	@Test
	public void oneComaTwoUnionThreeComaFour() {
		Expression expression = createASTFromExpression("(1, 2) union (3, 4)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(struct(1, 2), struct(3, 4))", bagResult.toString());
	}

	// bag(1, 2, 3) minus bag(2, 3) -> 1
	@Test
	public void bagOneTwoThreeMinusBagTwoThree() {
		Expression expression = createASTFromExpression("bag(1, 2, 3) minus bag(2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(1, (int) integerResult.getValue());

		assertEquals("1", integerResult.toString());
	}

	// 1 minus 1 -> bag()
	@Test
	public void oneMinusOne() {
		Expression expression = createASTFromExpression("1 minus 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// (1, 2) minus (2, 3) -> struct(1, 2)
	@Test
	public void oneComaTwoMinusTwoComaThree() {
		Expression expression = createASTFromExpression("(1, 2) minus (2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult structResult = (StructResult) result;

		Collection<AbstractQueryResult> structElements = structResult.getElements();

		assertEquals(2, (int) structElements.size());

		assertEquals("struct(1, 2)", structResult.toString());
	}

	// (1, 2) minus (1, 2) -> bag()
	@Test
	public void oneComaTwoMinusOneComaTwo() {
		Expression expression = createASTFromExpression("(1, 2) minus (1, 2)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(0, (int) bagElements.size());

		assertEquals("bag()", bagResult.toString());
	}

	// bag("ala", 2, 3) minus bag(2, 3.40) -> bag("ala", 3)
	@Test
	public void bagAlaComaTwoComaThreeMinusBagTwoComaThreePointFourty() {
		Expression expression = createASTFromExpression("bag(\"ala\", 2, 3) minus bag(2, 3.40)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		Collection<AbstractQueryResult> bagElements = bagResult.getElements();

		assertEquals(2, (int) bagElements.size());

		assertEquals("bag(\"ala\", 3)", bagResult.toString());
	}

	// bag(booleanValue, 3) minus bag(booleanValue) -> 3
	@Test
	public void bagBooleanValueComaThreeMinusBagBooleanValue() {
		Expression expression = createASTFromExpression("bag(booleanValue, 3) minus bag(booleanValue)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(3, (int) integerResult.getValue());

		assertEquals("3", integerResult.toString());
	}

	// forall 1 true -> true
	@Test
	public void forallOnetrue() {
		Expression expression = createASTFromExpression("forall 1 true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forall (bag(1, bag(2, 3) group as wew) as num) (num == 2) -> false
	@Test
	public void forallBagOneBagTwoThreeGroupAsWewAsNumNumEqualsTwo() {
		Expression expression = createASTFromExpression("forall (bag(1, bag(2, 3) group as wew) as num) (num == 2)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// forall (bag(1, bag(2, 3) group as wew) as num) (num == 1) -> Exception
	@Test(expected = RuntimeException.class)
	public void forallBagOneBagTwoThreeGroupAsWewAsNumNumEqualsOne() {
		Expression expression = createASTFromExpression("forall (bag(1, bag(2, 3) group as wew) as num) (num == 1)");
		expression.accept(visitor);
	}

	// forall emp married -> true
	@Test
	public void forallEmpMarried() {
		Expression expression = createASTFromExpression("forall emp married");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany 1 true
	@Test
	public void foranyOnetrue() {
		Expression expression = createASTFromExpression("forany 1 true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany (bag(1, bag(2, 3) group as wew) as num) (num == 2) -> Exception
	@Test(expected = RuntimeException.class)
	public void foranyBagOneBagTwoThreeGroupAsWewAsNumNumEqualsTwo() {
		Expression expression = createASTFromExpression("forany (bag(1, bag(2, 3) group as wew) as num) (num == 2)");
		expression.accept(visitor);
	}

	// forany (bag(1, bag(2, 3) group as wew) as num) (num == 1) -> true
	@Test
	public void foranyBagOneBagTwoThreeGroupAsWewAsNumNumEqualsOne() {
		Expression expression = createASTFromExpression("forany (bag(1, bag(2, 3) group as wew) as num) (num == 1)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// forany emp married -> true
	@Test
	public void foranyEmpMarried() {
		Expression expression = createASTFromExpression("forany emp married");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// unique(bag(1, 2, 1)) -> bag(1, 2)
	@Test
	public void uniqueBagOneTwoOne() {
		Expression expression = createASTFromExpression("unique(bag(1, 2, 1))");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		assertEquals(2, bagResult.getElements().size());

		assertEquals("bag(1, 2)", bagResult.toString());
	}

	// unique(bag(1.01, 2, 1.01, "ala")) -> bag(1.01, 2, "ala")
	@Test
	public void uniqueBagOnePointZeroOneTwoOnePointZeroOneAla() {
		Expression expression = createASTFromExpression("unique(bag(1.01, 2, 1.01, \"ala\"))");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResult = (BagResult) result;

		assertEquals(3, bagResult.getElements().size());

		assertEquals("bag(1.01, 2, \"ala\")", bagResult.toString());
	}

	// empty(1) -> false
	@Test
	public void emptyOne() {
		Expression expression = createASTFromExpression("empty(1)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// empty(bag(1.01, 2.35, 3))
	@Test
	public void emptyBagOnePointZeroOneTwoPointThirtyFiveThree() {
		Expression expression = createASTFromExpression("empty(bag(1.01, 2.35, 3))");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// empty(1 where false) -> true
	@Test
	public void emptyOneWhereFalse() {
		Expression expression = createASTFromExpression("empty(1 where false)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// bag(2, 3) in bag(1, 2, 3) -> true
	@Test
	public void bagTwoThreeInBagOneTwoThree() {
		Expression expression = createASTFromExpression("bag(2, 3) in bag(1, 2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// 1 in 1 -> true
	@Test
	public void oneInOne() {
		Expression expression = createASTFromExpression("1 in 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}

	// (1, 2) in (2, 3) -> false
	@Test
	public void oneTwoInTwoThree() {
		Expression expression = createASTFromExpression("(1, 2) in (2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(false, booleanResult.getValue());
	}

	// (1, 2) in (1, 2) -> true
	@Test
	public void oneTwoInOneTwo() {
		Expression expression = createASTFromExpression("(1, 2) in (1, 2)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult booleanResult = (BooleanResult) result;

		assertEquals(true, booleanResult.getValue());
	}
}
