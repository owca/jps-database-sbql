package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.List;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.arithmetic.AddExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class StructComaBagTests extends ExpressionsTest {

	// bag(1)
	@Test
	public void bagWithOne() {
		Expression expression = createASTFromExpression("bag(1)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(1, elements.size());

		assertEquals("bag(1)", bag.toString());
	}

	// bag(1, 2, 3)
	@Test
	public void bagWithOneTwoThree() {
		Expression expression = createASTFromExpression("bag(1, 2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(3, elements.size());

		assertEquals("bag(1, 2, 3)", bag.toString());
	}

	// bag(1 + 2, 3) -> bag(3, 3)
	@Test
	public void bagWithOnePlusTwoComaThree() {
		Expression expression = createASTFromExpression("bag(1 + 2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(2, elements.size());

		assertEquals("bag(3, 3)", bag.toString());
	}

	// bag(bag(1, 2, 3)) -> bag(1, 2, 3)
	@Test
	public void bagBagOneTwoThree() {
		Expression expression = createASTFromExpression("bag(bag(1, 2, 3))");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(3, elements.size());

		assertEquals("bag(1, 2, 3)", bag.toString());
	}

	// (1, 2) -> struct(1, 2)
	@Test
	public void oneTwoStructTest() {
		Expression expression = createASTFromExpression("(1, 2)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult resultAsStruct = (StructResult) result;
		List<AbstractQueryResult> structElements = resultAsStruct.getElements();

		assertEquals(2, resultAsStruct.getElements().size());

		assertEquals(1, (int) ((IntegerResult) structElements.get(0)).getValue());
		assertEquals(2, (int) ((IntegerResult) structElements.get(1)).getValue());

		assertEquals("struct(1, 2)", result.toString());
	}

	// struct(1)
	@Test
	public void structWithOne() {
		Expression expression = createASTFromExpression("struct(1)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult struct = (StructResult) result;

		Collection<AbstractQueryResult> elements = struct.getElements();

		assertEquals(1, elements.size());

		assertEquals("struct(1)", struct.toString());
	}

	// struct(1, 2, 3)
	@Test
	public void structWithOneTwoThree() {
		Expression expression = createASTFromExpression("struct(1, 2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult struct = (StructResult) result;

		Collection<AbstractQueryResult> elements = struct.getElements();

		assertEquals(3, elements.size());

		assertEquals("struct(1, 2, 3)", struct.toString());
	}

	// struct(1 + 2, 3)
	@Test
	public void structWithOnePlusTwoComaThree() {
		Expression expression = createASTFromExpression("struct(1 + 2, 3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult struct = (StructResult) result;

		Collection<AbstractQueryResult> elements = struct.getElements();

		assertEquals(2, elements.size());

		assertEquals("struct(3, 3)", struct.toString());
	}

	// struct(struct(1, 2, 3))
	@Test
	public void structStructOneTwoThree() {
		Expression expression = createASTFromExpression("struct(struct(1, 2, 3))");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult struct = (StructResult) result;

		Collection<AbstractQueryResult> elements = struct.getElements();

		assertEquals(3, elements.size());

		assertEquals("struct(1, 2, 3)", struct.toString());
	}

	// bag(1, 2), 3
	@Test
	public void bagOneTwoComaThree() {
		Expression expression = createASTFromExpression("bag(1, 2), 3");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(2, elements.size());

		assertEquals("bag(struct(1, 3), struct(2, 3))", bag.toString());
	}

	// bag(1, 2), bag(3, 4)
	@Test
	public void bagOneTwoComaBagThreeFour() {
		Expression expression = createASTFromExpression("bag(1, 2), bag(3, 4)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bag = (BagResult) result;

		Collection<AbstractQueryResult> elements = bag.getElements();

		assertEquals(4, elements.size());

		assertEquals("bag(struct(1, 3), struct(1, 4), struct(2, 3), struct(2, 4))", bag.toString());
	}

	// 1, 2.2, false, "napis" -> struct(1, 2.2, false, napis)
	@Test
	public void oneComaTwoPointTwoComaFalseComaNapis() {
		Expression expression = createASTFromExpression("1, 2.2, false, \"napis\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);
		StructResult ex1StructResult = (StructResult) result;

		List<AbstractQueryResult> ex1Elements = ex1StructResult.getElements();

		assertEquals(4, ex1Elements.size());

		assertTrue(ex1Elements.get(0) instanceof IntegerResult);
		assertEquals((int) ((IntegerResult) ex1Elements.get(0)).getValue(), 1);

		assertTrue(ex1Elements.get(1) instanceof DoubleResult);
		assertEquals((double) ((DoubleResult) ex1Elements.get(1)).getValue(), 2.2, 0.001);

		assertTrue(ex1Elements.get(2) instanceof BooleanResult);
		assertEquals((boolean) ((BooleanResult) ex1Elements.get(2)).getValue(), false);

		assertTrue(ex1Elements.get(3) instanceof StringResult);
		assertEquals((String) ((StringResult) ex1Elements.get(3)).getValue(), "napis");

		assertEquals("struct(1, 2.2, false, \"napis\")", result.toString());
	}

	// 1 join 2 -> struct(1, 2)
	@Test
	public void oneJoinTwo() {
		Expression expression = createASTFromExpression("1 join 2");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult resultAsStructure = (StructResult) result;

		List<AbstractQueryResult> expressionElements = resultAsStructure.getElements();

		assertEquals(2, expressionElements.size());

		assertTrue(expressionElements.get(0) instanceof IntegerResult);
		assertEquals((int) ((IntegerResult) expressionElements.get(0)).getValue(), 1);

		assertTrue(expressionElements.get(1) instanceof IntegerResult);
		assertEquals((int) ((IntegerResult) expressionElements.get(1)).getValue(), 2);

		assertEquals("struct(1, 2)", result.toString());
	}

	// (1 as n) join n -> struct(<n, 1>, 1)
	@Test
	public void oneAsNJoinN() {
		Expression expression = createASTFromExpression("(1 as n) join n");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StructResult);

		StructResult resultAsStructure = (StructResult) result;

		List<AbstractQueryResult> expressionElements = resultAsStructure.getElements();

		assertEquals(2, expressionElements.size());

		assertTrue(expressionElements.get(0) instanceof Binder);
		assertEquals("n", ((Binder) expressionElements.get(0)).getName());

		assertTrue(expressionElements.get(1) instanceof IntegerResult);
		assertEquals(1, (int) ((IntegerResult) expressionElements.get(1)).getValue());

		assertEquals("struct(<n, 1>, 1)", result.toString());
	}

	// emp join married
	@Test
	public void empJoinmarried() {
		Expression expression = createASTFromExpression("emp join married");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult resultBag = (BagResult) result;

		Collection<AbstractQueryResult> expressionElements = resultBag.getElements();

		assertEquals(2, expressionElements.size());
	}

	// (emp as e) join (e.married)
	@Test
	public void empAsEJoinEMarried() {
		Expression expression = createASTFromExpression("(emp as e) join (e.married)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult resultBag = (BagResult) result;

		Collection<AbstractQueryResult> expressionElements = resultBag.getElements();

		assertEquals(2, expressionElements.size());
	}
}
