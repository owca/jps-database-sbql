package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;
import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.arithmetic.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.*;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class ArithmeticTests extends ExpressionsTest {

	// 10 / 5 -> 2
	@Test
	public void tenDividedByFive() {
		Expression expression = createASTFromExpression("10 / 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(2, (int) integerResult.getValue());

		assertEquals("2", integerResult.toString());
	}

	// 5/3.50 -> 1.4285714285714286
	@Test
	public void fiveDividedByThreePointFifty() {
		Expression expression = createASTFromExpression("5 / 3.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(1.4285714285714286, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50/5 -> 0.7
	@Test
	public void threePointFiftyDividedByFive() {
		Expression expression = createASTFromExpression("3.50 / 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(0.7, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50/5.50 -> 0.63636363636364
	@Test
	public void threePointFiftyDividedByFivePointFifty() {
		Expression expression = createASTFromExpression("3.50 / 5.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(0.63636363636364, (double) doubleResult.getValue(), 0.001);
	}

	// 10 - 5 -> 5
	@Test
	public void tenMinusFive() {
		Expression expression = createASTFromExpression("10 - 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(5, (int) integerResult.getValue());

		assertEquals("5", integerResult.toString());
	}

	// 5 - 3.50 -> 1.5
	@Test
	public void fiveMinusThreePointFifty() {
		Expression expression = createASTFromExpression("5 - 3.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(1.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 - 5 -> -1.5
	@Test
	public void threePointFiftyMinusByFive() {
		Expression expression = createASTFromExpression("3.50 - 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(-1.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 - 5.50 -> -2
	@Test
	public void threePointFiftyminusdByFivePointFifty() {
		Expression expression = createASTFromExpression("3.50 - 5.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(-2, (int) integerResult.getValue());
	}

	// 10 * 5 -> 50
	@Test
	public void tenTimesFive() {
		Expression expression = createASTFromExpression("10 * 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(50, (int) integerResult.getValue());

		assertEquals("50", integerResult.toString());
	}

	// 5 * 3.50 -> 17.5
	@Test
	public void fiveTimesThreePointFifty() {
		Expression expression = createASTFromExpression("5 * 3.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(17.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 * 5 -> 17.5
	@Test
	public void threePointFiftyTimesFive() {
		Expression expression = createASTFromExpression("3.50 * 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(17.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 * 5.50 -> 19.25
	@Test
	public void threePointFiftyTimesFivePointFifty() {
		Expression expression = createASTFromExpression("3.50 * 5.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(19.25, (double) doubleResult.getValue(), 0.001);
	}

	// 10 % 5 -> 0
	@Test
	public void tenModuloFive() {
		Expression expression = createASTFromExpression("10 % 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(0, (int) integerResult.getValue());

		assertEquals("0", integerResult.toString());
	}

	// 5 % 3.50 -> 1.5
	@Test
	public void fiveModuloThreePointFifty() {
		Expression expression = createASTFromExpression("5 % 3.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(1.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 % 5 -> 3.5
	@Test
	public void threePointFiftyModuloFive() {
		Expression expression = createASTFromExpression("3.50 % 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(3.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 % 5.50 -> 3.5
	@Test
	public void threePointFiftyModuloFivePointFifty() {
		Expression expression = createASTFromExpression("3.50 % 5.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(3.5, (double) doubleResult.getValue(), 0.001);
	}

	// 10 + 5 -> 15
	@Test
	public void tenPlusFive() {
		Expression expression = createASTFromExpression("10 + 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(15, (int) integerResult.getValue());

		assertEquals("15", integerResult.toString());
	}

	// 5 + 3.50 -> 8.5
	@Test
	public void fivePlusThreePointFifty() {
		Expression expression = createASTFromExpression("5 + 3.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(8.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 + 5 -> 8.5
	@Test
	public void threePointFiftyPlusFive() {
		Expression expression = createASTFromExpression("3.50 + 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(8.5, (double) doubleResult.getValue(), 0.001);
	}

	// 3.50 + 5.50 -> 9
	@Test
	public void threePointFiftyPlusFivePointFifty() {
		Expression expression = createASTFromExpression("3.50 + 5.50");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(9, (int) integerResult.getValue());

		assertEquals("9", integerResult.toString());
	}

	// 3 + "Ala" -> "3Ala"
	@Test
	public void threePlusAla() {
		Expression expression = createASTFromExpression("3 + \"Ala\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StringResult);

		StringResult stringResult = (StringResult) result;

		assertEquals("3Ala", (String) stringResult.getValue());

		assertEquals("\"3Ala\"", stringResult.toString());
	}

	// 3.5 + "Ala" -> "3.5Ala"
	@Test
	public void threePointFivePlusAla() {
		Expression expression = createASTFromExpression("3.50 + \"Ala\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StringResult);

		StringResult stringResult = (StringResult) result;

		assertEquals("3.5Ala", (String) stringResult.getValue());

		assertEquals("\"3.5Ala\"", stringResult.toString());
	}

	// "Ala" + 3.7 -> "Ala3.7"
	@Test
	public void alaPlusThreePointSeven() {
		Expression expression = createASTFromExpression("\"Ala\" + 3.7");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StringResult);

		StringResult stringResult = (StringResult) result;

		assertEquals("Ala3.7", (String) stringResult.getValue());

		assertEquals("\"Ala3.7\"", stringResult.toString());
	}

	// true + "Ala" -> "trueAla"
	@Test
	public void truePlusAla() {
		Expression expression = createASTFromExpression("true + \"Ala\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof StringResult);

		StringResult stringResult = (StringResult) result;

		assertEquals("trueAla", (String) stringResult.getValue());

		assertEquals("\"trueAla\"", stringResult.toString());
	}

	// -(3.3) -> -3.3
	@Test
	public void minusThreePointThree() {
		Expression expression = createASTFromExpression("-(3.3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(-3.3, (double) doubleResult.getValue(), 0.001);
	}

	// -(-3.3) -> 3.3
	@Test
	public void minusMinusThreePointThree() {
		Expression expression = createASTFromExpression("-(-3.3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(3.3, (double) doubleResult.getValue(), 0.001);
	}

	// -(11) -> -11
	@Test
	public void minusEleven() {
		Expression expression = createASTFromExpression("-(11)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(-11, (int) integerResult.getValue());
	}

	// -(-11) -> 11
	@Test
	public void minusMinusEleven() {
		Expression expression = createASTFromExpression("-(-11)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(11, (int) integerResult.getValue());
	}

	// +(11) -> 11
	@Test
	public void plusEleven() {
		Expression expression = createASTFromExpression("+(11)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof IntegerResult);

		IntegerResult integerResult = (IntegerResult) result;

		assertEquals(11, (int) integerResult.getValue());
	}

	// +(3.3) -> 3.3
	@Test
	public void plusThreePointThree() {
		Expression expression = createASTFromExpression("+(3.3)");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof DoubleResult);

		DoubleResult doubleResult = (DoubleResult) result;

		assertEquals(3.3, (double) doubleResult.getValue(), 0.001);
	}
}
