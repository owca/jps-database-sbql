package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.*;
import info.owczarek.jps.database.ast.auxiliary.AsExpression;
import info.owczarek.jps.database.ast.auxiliary.GroupAsExpression;
import info.owczarek.jps.database.ast.binary.ComaExpression;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.qres.AbstractQueryResult;
import info.owczarek.jps.database.qres.BagResult;
import info.owczarek.jps.database.qres.Binder;
import info.owczarek.jps.database.qres.IntegerResult;
import info.owczarek.jps.database.qres.ReferenceResult;
import info.owczarek.jps.database.qres.StructResult;
import info.owczarek.jps.database.store.ComplexObject;
import info.owczarek.jps.database.store.SBAObject;
import info.owczarek.jps.database.store.SimpleObject;

public class NameTests extends ExpressionsTest {

	// integerNumber -> ref(10)
	@Test
	public void integerNumber() {
		Expression expression = createASTFromExpression("integerNumber");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult resultReference = (ReferenceResult) result;
		SBAObject referencedObject = sbaStore.retrieve(resultReference.getOIDValue());

		assertTrue(referencedObject instanceof SimpleObject<?>);

		SimpleObject<Integer> referencedInteger = (SimpleObject<Integer>) referencedObject;

		assertEquals(10, (int) referencedInteger.getValue());
	}

	// realNumber -> ref(234.35)
	@Test
	public void realNumber() {
		Expression expression = createASTFromExpression("realNumber");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult resultReference = (ReferenceResult) result;
		SBAObject referencedObject = sbaStore.retrieve(resultReference.getOIDValue());

		assertTrue(referencedObject instanceof SimpleObject<?>);

		SimpleObject<Double> referencedDouble = (SimpleObject<Double>) referencedObject;

		assertEquals(234.35, (double) referencedDouble.getValue(), 0.001);
	}

	// booleanValue -> ref(true)
	@Test
	public void booleanValue() {
		Expression expression = createASTFromExpression("booleanValue");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult resultReference = (ReferenceResult) result;
		SBAObject referencedObject = sbaStore.retrieve(resultReference.getOIDValue());

		assertTrue(referencedObject instanceof SimpleObject<?>);

		SimpleObject<Boolean> referencedBoolean = (SimpleObject<Boolean>) referencedObject;

		assertEquals(true, (boolean) referencedBoolean.getValue());
	}

	// stringValue -> ref("Ala")
	@Test
	public void stringValue() {
		Expression expression = createASTFromExpression("stringValue");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult resultReference = (ReferenceResult) result;
		SBAObject referencedObject = sbaStore.retrieve(resultReference.getOIDValue());

		assertTrue(referencedObject instanceof SimpleObject<?>);

		SimpleObject<String> referencedString = (SimpleObject<String>) referencedObject;

		assertEquals("Ala", (String) referencedString.getValue());
	}

	// pomidor -> bag(ref(), ref(), ref(), ref())
	@Test
	public void pomidor() {
		Expression expression = createASTFromExpression("pomidor");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult referencesBag = (BagResult) result;

		Collection<AbstractQueryResult> references = referencesBag.getElements();

		assertEquals(4, references.size());
	}

	// sampleComplexObj -> ref(sampleComplexObj)
	@Test
	public void sampleComplexObj() {
		Expression expression = createASTFromExpression("sampleComplexObj");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof ReferenceResult);

		ReferenceResult resultReference = (ReferenceResult) result;
		SBAObject referencedObject = sbaStore.retrieve(resultReference.getOIDValue());

		assertTrue(referencedObject instanceof ComplexObject);
	}

	// 1 as liczba -> <liczba, 1>
	@Test
	public void oneAsLiczba() {
		Expression expression = createASTFromExpression("1 as liczba");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		Binder resultBinder = (Binder) result;

		assertEquals("liczba", resultBinder.getName());

		assertTrue(resultBinder.getValue() instanceof IntegerResult);

		assertEquals("<liczba, 1>", result.toString());
	}

	// bag(1, 2) as num -> bag(<num, 1>, <num, 2>)
	@Test
	public void bagOneComaTwoAsNum() {
		Expression expression = createASTFromExpression("bag(1, 2) as num");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BagResult);

		BagResult bagResulg = (BagResult) result;

		assertEquals("bag(<num, 1>, <num, 2>)", result.toString());
	}

	// bag(2) as num
	@Test
	public void bagTwoAsNum() {
		Expression expression = createASTFromExpression("bag(2) as num");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		Binder resultBinder = (Binder) result;

		assertEquals("<num, 2>", resultBinder.toString());
	}

	// (1, 2) as num
	@Test
	public void OneComaTwoAsNum() {
		Expression expression = createASTFromExpression("(1, 2) as num");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		assertEquals("<num, struct(1, 2)>", result.toString());
	}

	// bag(1, 2, 3) group as num
	@Test
	public void bagOneTwoThreeGroupAsNum() {
		Expression expression = createASTFromExpression("bag(1, 2, 3) group as num");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		Binder resultBinder = (Binder) result;

		assertEquals("<num, bag(1, 2, 3)>", resultBinder.toString());
	}

	// 1 group as liczba -> <liczba, 1>
	@Test
	public void oneGroupAsLiczba() {
		Expression expression = createASTFromExpression("1 group as liczba");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof Binder);

		Binder resultBinder = (Binder) result;

		assertEquals("liczba", resultBinder.getName());

		assertTrue(resultBinder.getValue() instanceof IntegerResult);

		assertEquals("<liczba, 1>", result.toString());
	}
}
