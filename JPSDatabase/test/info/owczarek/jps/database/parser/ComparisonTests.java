package info.owczarek.jps.database.parser;

import static org.junit.Assert.*;

import java.util.List;

import info.owczarek.jps.database.ExpressionsTest;
import info.owczarek.jps.database.ast.Expression;
import info.owczarek.jps.database.ast.binary.*;
import info.owczarek.jps.database.ast.binary.comparison.*;
import info.owczarek.jps.database.ast.terminal.*;
import info.owczarek.jps.database.ast.unary.BagExpression;
import info.owczarek.jps.database.qres.*;

import org.junit.Test;

public class ComparisonTests extends ExpressionsTest {

	// integerNumber == 10 -> true
	@Test
	public void integerNumberEqualsToTen() {
		Expression expression = createASTFromExpression("integerNumber == 10");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// bag(1,2) == 2 -> exception
	@Test(expected = RuntimeException.class)
	public void bagOneTwoEqualsToTwo() {
		Expression expression = createASTFromExpression("bag(1,2) == 2");
		expression.accept(visitor);
		fail();
	}

	// booleanValue == true -> true
	@Test
	public void booleanValueEqualsToTrue() {
		Expression expression = createASTFromExpression("booleanValue == true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// stringValue == "Ala" -> true
	@Test
	public void stringValueEqualsToAla() {
		Expression expression = createASTFromExpression("stringValue == \"Ala\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 == 2 -> false
	@Test
	public void oneEqualsToTwo() {
		Expression expression = createASTFromExpression("1 == 2");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 1 == true -> false
	@Test
	public void oneEqualsToTrueTest() {
		Expression expression = createASTFromExpression("1 == true");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 5 == "5" -> false
	@Test
	public void fiveEqualsToStringFiveTest() {
		Expression expression = createASTFromExpression("5 == \"5\"");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 5.50 == 5 -> false
	@Test
	public void fivePointFiftyEqualsToFiveTest() {
		Expression expression = createASTFromExpression("5.50 == 5");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 1 > 1 -> false
	@Test
	public void oneGreaterThanOne() {
		Expression expression = createASTFromExpression("1 > 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 3 > 2.99 -> true
	@Test
	public void threeGreaterThanTwoPointNinetyNine() {
		Expression expression = createASTFromExpression("3 > 2.99");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.35 > 24.34 -> true
	@Test
	public void twentyFourPointThirtyFiveGreaterThanTwentyFourPointThirtyFour() {
		Expression expression = createASTFromExpression("24.35 > 24.34");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 >= 1 -> true
	@Test
	public void oneGreaterOrEqualToOne() {
		Expression expression = createASTFromExpression("1 >= 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 3 >= 2.99 -> true
	@Test
	public void threeGreaterOrEqualThanTwoPointNinetyNine() {
		Expression expression = createASTFromExpression("3 >= 2.99");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.35 >= 24.34 -> true
	@Test
	public void twentyFourPointThirtyFiveGreaterOrEqualThanTwentyFourPointThirtyFour() {
		Expression expression = createASTFromExpression("24.35 >= 24.34");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 <= 1 -> true
	@Test
	public void oneLesserOrEqualToOne() {
		Expression expression = createASTFromExpression("1 <= 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 24.34 <= 24.35 -> true
	@Test
	public void twentyFourPointThirtyFourLesserOrEqualToTwentyFourPointThirtyFive() {
		Expression expression = createASTFromExpression("24.34 <= 24.35");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 2.99 <= 3 -> true
	@Test
	public void twoPointNinetyNineLesserOrEqualToThree() {
		Expression expression = createASTFromExpression("2.99 <= 3");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 1 < 1 -> false
	@Test
	public void oneLesserThanOne() {
		Expression expression = createASTFromExpression("1 < 1");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(false, resultAsBoolean.getValue());

		assertEquals("false", resultAsBoolean.toString());
	}

	// 24.34 < 24.35 -> true
	@Test
	public void twentyFourPointThirtyFourLesserThanTwentyFourPointThirtyFive() {
		Expression expression = createASTFromExpression("24.34 < 24.35");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

	// 2.99 < 3 -> true
	@Test
	public void twoPointNinetyNineLesserThanThree() {
		Expression expression = createASTFromExpression("2.99 < 3");
		expression.accept(visitor);
		AbstractQueryResult result = qres.pop();
		System.out.println(expression + " -> " + result.toString());

		assertTrue(result instanceof BooleanResult);

		BooleanResult resultAsBoolean = (BooleanResult) result;

		assertEquals(true, resultAsBoolean.getValue());

		assertEquals("true", resultAsBoolean.toString());
	}

}
